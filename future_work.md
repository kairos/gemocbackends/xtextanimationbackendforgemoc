# Future work ideas

### If you want to contribute to this project but have no idea where to start, here are a few leads that you can work on. Maybe it can inspire you instead.  

***

### Adding new user actions 

At the moment, hovers can appear in two different ways :  
- when the debugger is launched  
- when double clicking a concept.   
This is done thanks to the _**XtextAnimationMouseListener**_, that implements the SWT Mouse Listener. This listener responds to mouse clicks : single click, mouse up or mouse down, or double click. A user might want to specify how they want to activate hovers : on single click, or double click, or maybe with a key. This option can be added to RunStaR, and the current listener can be used. For keyboard action, we will need to use a different listener.  
Other animations could also be triggered by user actions.


### Editing text
When a comment displays a variable's current value, we might want to edit the presented value to see what happens. We can image having a hover displaying which language concepts will be impacted by the change, or just proceed to the next step and the program takes into account the changes.  
Code Minings do not allow edition : they are untouchable text. In order to edit a comment, it must have been written in the editor. You can do so by using the replace() method of the XtextDocument, that belongs to the XtextEditor (EditorUtils.getActiveXtextEditor() inside a thread).  
However, after using this method, we have to find a way to reparse the editor, because the offsets (object position inside the document) are not automatically updated. You can have a look at the
[old Commenter](https://gitlab.inria.fr/kairos/gemocbackends/xtextanimationbackendforgemoc/-/blob/develop/org.eclipse.gemoc.addon.xtextanimator.ui/src/org/eclipse/gemoc/addon/xtextanimator/ui/old/Commenter.java).

### Marker customization
At the moment, only three marker icons are available, those of Xtext. In order to customize a marker's icon, a hack has been proposed [here](https://www.eclipse.org/forums/index.php/m/1842969/#msg_1842969).


### SWT Browser
Java SWT tools allow us to do many things inside hovers. However, current hovers are not "very stylish", and can be upgraded ! Feel free to customize them.


### Multiple values in the same chart
At the moment, only one ''y value' is drawn on the charts. Changes inside the ChartAnimator & the RunStaRConfigurationRetriever to take multiple values in the same chart.

### Retrieve language id 
The language id, such as "org.lflang.LF" for DSL LinguaFranca, is used by the CommenterAnimator to write comments in the language format. However, I am unsure of the way to properly retrieve the language id : it must be done in the RunStaRConfigurationRetriever.

### RunToLine
There is a currently unused animation called RunToLine, that brings you to a specific line. It seemed like a good idea at first, but it was not used in the end. You can add it to RunStaR if you find this feature interesting to have.
