# XtextAnimationBackendForGemoc


#### When executing a program, its state varies. It is thus interesting to have a debugger representing these runtime changes. The following backend provides textual animations for domain-specific languages (DSL), proposing different representations of the runtime state of the program. Mainly using [Xtext](https://www.eclipse.org/Xtext/) features, the project is available as a [GEMOC Studio](https://gemoc.org/studio) add-on.
  
***

## Description of the project

The XtextAnimationBackendForGemoc provides textual animations as a GEMOC add-on. When developing their DSL, a language designer can add the XtextAnimation plugin to create a debugger. They will be able to simply customize it with a configuration file, written in [RunStaR](https://gitlab.inria.fr/kairos/gemocbackends/runstar) DSL. [Here](https://gitlab.inria.fr/kairos/gemocbackends/xtextanimationbackendforgemoc/-/blob/develop/runstar_example.runstar) is an example of a RunStaR configuration file. Currently, text animations include:  
- Permanent hovers: displays HTML text, Javascript code and images. Images can be a time series that displays an integer-based variable's evolution through time.  
- Highlighting: when a concept is changed at runtime, it will be highlighted in the editor.  
- Comments: text can be displayed next to a changed concept.  
- Markers: information can be displayed inside an Eclipse marker in the editor margin

Future work ideas can be found [here](https://gitlab.inria.fr/kairos/gemocbackends/xtextanimationbackendforgemoc/-/blob/develop/future_work.md).

 
### Main classes and their responsabilities

Class       |  Description
----------  |  ----------
XtextAnimationBehaviorManager _(implements **IEngineAddon**)_ | Provided through the *org.eclipse.gemoc.gemoc\_language\_workbench.engine_addon* extension point. It contains methods that depend on the engine state (started, about to stop...). The behavior manager contains all of the configured animations and calls their methods when needed. 
XtextAnimator _(abstract)_ | Contains the essential objects needed by an animation : the EObject to animate, an ExecutionEngine to retrieve DSL information. To know when the animation must be executed, the **XtextAnimator** also uses a GemocClockEntity (a clock trigger) and a boolean indicating the engine state. There are two methods to override: *updates()* and *cleanUp()*. A third method, _checkAuxClocks()_ can also be overriden but it is not mandatory. This method is used by Animators that have multiple EObjects to animate.
RunStaRConfigurationRetriever  | Retrieves the **RunStaR configuration file** written by the language designer and builds the different animation *Properties*.
Properties _(abstract)_ | Set by the **RunStaRConfigurationRetriever**, a Properties object contains all the information needed to create a **XtextAnimator**, according to the **RunStaR configuration file**. When the parameters are retrieved and set, non-mandatory parameters are set to their default values. Afterwards, it is possible to use the *createAnimator()* method to create retrieve the fully set **XtextAnimator**.
XtextAnimationMouseListener _(implements **MouseListener**)_| Some actions can be triggered by user actions, such as double clicking a line in the editor. You can add the needed animations in this mouse listener's methods.
HoverMouseLinker | Sets up and provides the XtextAnimationMouseListener to the XtextEditor's XtextSourceViewer.
  
  
![class_diagram](Class_diagram.jpg "XtextAnimationBackendForGemoc simplified class diagram")


### Adding a new animation
In order to add a new animation called _MyAnimation_, you will need to :  
- Create your _MyAnimationAnimator_ class extending **XtextAnimator**  
- Create the _MyAnimationProperties_ extending **Properties**  
- If your animation has specific needs, you can add them in the _createAssociatedAnimators()_ method of the **XtextAnimationBehaviorManager**. The **HoverManagerAnimator** has an example of such behavior.  
- If your animation has global actions, make them static and call them in the appropriate method of the **XtextAnimationBehaviorManager**. The **HighlightingAnimator** has an example of such behavior.



## Current animations : how they work

### Permanent Hovers : HoverAnimator
Permanent Hovers are SWT widgets such as a Browser or a Label, configured with the properties' contents and set to visible when needed. These widgets need to be provided with a Shell. Every shell modification must happen inside 

```
Display.getDefault().syncExec(new Runnable() {  
	@Override  
	public void run() {  
		//your code here  
	}  
});  
```

Be careful : when updating a widget, you indirectly update the shell it is contained in. Thus, be sure that every widget update is also contained in a thread as shwon above.  
Hovers appear when double clicking an entity that has Hover Properties. To do so, a **HoverMouseLinker**, set in the **XtextAnimationBehaviorManager**, retrieves all the **HoverAnimator** and provides them to a mouse listener: the **XtextAnimationMouseListener**. It implements the SWT MouseListener.  
To customize displayed text, the HoverAnimator has a HTMLBuilder. It builds a string in HTML format, following the instructions given by the RunStaR configuration file.

### Comments : CommenterAnimator
Comments exploit the code mining feature of Xtext. Each **CommenterAnimator** has its own **XtextAnimationCodeMiningProvider**, that extends the **AbstractXtextCodeMiningProvider**. The CommenterAnimator is responsible of creating its XtextAnimationCodeMiningProvider and stores it in a static list. When all the providers are created, the CommenterAnimator can statically give them to the XtextEditor's SourceViewer, that responsible of drawing the code minings.  
The CommenterAnimator is also responsible of telling its XtextAnimationCodeMiningProvider when it should update its contents.

### Highlight : HighlightingAnimator
To highlight an element, you will need to customize its StyleRange in the XtextEditor. As textual customisation overlaps syntax highlighting, to avoid conflicts we give all StyleRanges to a TextPresentation that solves overlapping issues.  

### Markers : MarkerAnimator
Markers are XtextAnnotation, that need to be provided to the XtextEditor's SourceViewer. It contains text and has an icon in the margin. Three default icons are provided by Xtext : Error, Warning and Information.

***
# Contributors

Copyright (c) INRIA Kairos and others.  
All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available on the [Eclipse Website](http://www.eclipse.org/legal/epl-v10.html).


[Julien Deantoni](https://gitlab.inria.fr/jdeanton),
[Ludovic Marti](https://gitlab.inria.fr/lumarti) &
[Ryana Karaki](https://gitlab.inria.fr/rkaraki) 
worked on this project.
