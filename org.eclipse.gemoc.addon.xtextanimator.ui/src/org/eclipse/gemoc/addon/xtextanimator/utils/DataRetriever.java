package org.eclipse.gemoc.addon.xtextanimator.utils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;

/*
 * To easily retrieve data in the different animations
 * from https://github.com/jdeantoni/LinguaFrancaInGemoc/
 * blob/9b00c5fed01f2e8591353e786d4091845caa4456/org.eclipse.gemoc.addon.klighdanimator/src/
 * org/eclipse/gemoc/addon/klighdanimator/DataRepresentationBehaviour.java#L143
 */
public class DataRetriever {
	
	private Class<?> RTDAccessor;
	private IExecutionEngine<?> ee;
	
	public DataRetriever(IExecutionEngine<?> ee) {
		this.ee=ee;
		RTDAccessor = retrieveRTDAccessor();
	}
	
	public Class<?> retrieveRTDAccessor() {
		String fullLanguageName = ee.getExecutionContext().getLanguageDefinitionExtension().getName();
		int lastDot = fullLanguageName.lastIndexOf(".");
		if (lastDot == -1)
			lastDot = 0;
		String languageName = fullLanguageName.substring(lastDot + 1);
		String languageToUpperFirst = languageName.substring(0, 1).toUpperCase() + languageName.substring(1);
		Class<?> res = null;
		try {
			res = ee.getExecutionContext().getDslBundle()
						.loadClass(languageToUpperFirst.toLowerCase() + ".xdsml.api.impl." + languageToUpperFirst
								+ "RTDAccessor");
			} catch (ClassNotFoundException
					| IllegalArgumentException | SecurityException e) {
				e.printStackTrace();
			}
		return res;
	}
	
	public Object retrieveObject(EObject associatedObject, String attributeTracked) {
		Resource resInEngine = ee.getExecutionContext().getResourceModel();
		EObject aOInEngine = resInEngine.getEObject(EcoreUtil.getURI(associatedObject).fragment());;
		return getAttribute(aOInEngine, attributeTracked);
	}
	
	public String retrieveObjectAsString(EObject associatedObject, String attributeTracked) {
		String result = attributeTracked;
		try {
			result += " : " + retrieveObject(associatedObject,attributeTracked);
		} catch (Exception e) {
			//ignore
		}
		return result;
	}
	
	public String retrieveMultipleValues(EObject associatedObject, List<String> attributesTracked) {
		String result = "";
		for(String attribute : attributesTracked) {
			result += retrieveObjectAsString(associatedObject, attribute);
			if(attributesTracked.indexOf(attribute) < attributesTracked.size()-1){
				result += "; ";
			}
		}
		return result;
	}
	
	public Object getAttribute(EObject eo, String attributeName) {
		Method attributeGetter = getGetter(attributeName, eo.getClass());
		Object res = null;
		try {
			res = attributeGetter.invoke(null, eo);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return res;
	}

	Method getGetter(String attributeName, Class<?> attributeType) {
		Method[] methods = RTDAccessor.getMethods();
		for(Method m : methods) {
			if (m.getName().equals("get"+attributeName) && m.getParameterTypes().length == 1 && m.getParameterTypes()[0].isAssignableFrom(attributeType)) {
				return m;
			}
		}
		throw new RuntimeException("no accessor method has been found for attribute "+attributeName+" in class "+RTDAccessor.getPackageName()+"::"+RTDAccessor.getName());
	}
	
	public String getName(EObject associatedObject) {
		String res = "";
		List<Class<?>> list = AllClassRetriever.bfs(associatedObject.getClass());
		List<Method> methods = new ArrayList<>();
		
		for(Class<?> c : list) {
			Method[] m = c.getDeclaredMethods();
			for(Method i : m) {
				methods.add(i);
			}
		}
		
		for(Method m : methods) {
			if(m.getName().compareTo("getName")==0 || m.getName().compareTo("getname")==0) {
				m.setAccessible(true);
				try {
					res = "" + m.invoke(associatedObject);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

}
