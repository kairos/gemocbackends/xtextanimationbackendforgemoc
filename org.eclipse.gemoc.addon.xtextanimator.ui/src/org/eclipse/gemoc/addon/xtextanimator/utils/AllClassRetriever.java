package org.eclipse.gemoc.addon.xtextanimator.utils;

import java.util.ArrayList;
import java.util.List;

public class AllClassRetriever {

	/*
	 * Retrieves all super classes linked to the actual class, and the actual class
	 */
	public static List<Class<?>> bfs(Class<?> c) {
		List<Class<?>> r = new ArrayList<>();
		List<Class<?>> q = new ArrayList<>();
		q.add(c);
		while (!q.isEmpty()) {
			c = q.remove(0);
			r.add(c);
			if (c.getSuperclass() != null) {
				q.add(c.getSuperclass());
			}
			for (Class<?> i : c.getInterfaces()) {
				q.add(i);
			}
		}
		return r;
	}
	
}
