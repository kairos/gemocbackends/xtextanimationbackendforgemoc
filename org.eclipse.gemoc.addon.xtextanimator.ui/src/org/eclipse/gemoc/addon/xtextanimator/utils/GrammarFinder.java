package org.eclipse.gemoc.addon.xtextanimator.utils;

import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ContentHandler;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.resource.IResourceServiceProvider;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.inject.ConfigurationException;

/**
 * If needed, this class allows one to retrieve
 * a grammar & its set of rules thanks to a languageID
 * At the moment, used by the GrammarCommentStyle to retrieve
 * the commenting style of the language
 */
public class GrammarFinder {

	public GrammarFinder() {}
	
	// The following methods belong to : https://www.programcreek.com/java-api-examples/?project_name=dsldevkit%2Fdsl-devkit
		/**
		 * Based on method getResourceServiceProviderById() we can retrieve the Grammar this way
		 * @return the grammar associated to the language ID
		 */
		public static IGrammarAccess retrieveGrammar(String languageID) {
			  ImmutableMap<Map<String, Object>, ? extends Function<String, IResourceServiceProvider>> resourceProvidersMap = getProviderMaps();
			  for (Map.Entry<Map<String, Object>, ? extends Function<String, IResourceServiceProvider>> mapEntry : resourceProvidersMap.entrySet()) {
			    Map<String, Object> map = mapEntry.getKey();
			    for (Map.Entry<String, Object> entry : map.entrySet()) {
			      try {
			        IResourceServiceProvider resourceServiceProvider = mapEntry.getValue().apply(entry.getKey());
			        if (resourceServiceProvider == null) {
			          continue;
			        }
			        IGrammarAccess grammarAccess = resourceServiceProvider.get(IGrammarAccess.class);
			        
			        if (grammarAccess != null && grammarAccess.getGrammar().getName().equals(languageID)) {
			          return grammarAccess;
			        }
			      } catch (ConfigurationException ex) {
			        // ignore
			      }
			    }
			  }
			  return null;
		}
		
		  /**
		   * Gets the maps of the various resource service providers from the registry.
		   *
		   * @return the resource service provider's maps
		   */
		  private static ImmutableMap<Map<String, Object>, ? extends Function<String, IResourceServiceProvider>> getProviderMaps() {
		    ImmutableMap<Map<String, Object>, ? extends Function<String, IResourceServiceProvider>> resourceProvidersMap = //
		    ImmutableMap.of(IResourceServiceProvider.Registry.INSTANCE.getExtensionToFactoryMap(), new Function<String, IResourceServiceProvider>() {
		      @Override
		      public IResourceServiceProvider apply(final String input) {
		        URI fake = URI.createURI("fake:/foo." + input); //$NON-NLS-1$
		        return IResourceServiceProvider.Registry.INSTANCE.getResourceServiceProvider(fake, ContentHandler.UNSPECIFIED_CONTENT_TYPE);
		      }
		    }, IResourceServiceProvider.Registry.INSTANCE.getContentTypeToFactoryMap(), new Function<String, IResourceServiceProvider>() {
		      @Override
		      public IResourceServiceProvider apply(final String input) {
		        URI fake = URI.createURI("fake:/foo"); //$NON-NLS-1$
		        return IResourceServiceProvider.Registry.INSTANCE.getResourceServiceProvider(fake, input);
		      }
		    }, IResourceServiceProvider.Registry.INSTANCE.getProtocolToFactoryMap(), new Function<String, IResourceServiceProvider>() {
		      @Override
		      public IResourceServiceProvider apply(final String input) {
		        URI fake = URI.createURI(input + ":/foo"); //$NON-NLS-1$
		        return IResourceServiceProvider.Registry.INSTANCE.getResourceServiceProvider(fake, ContentHandler.UNSPECIFIED_CONTENT_TYPE);
		      }
		    });
		    return resourceProvidersMap;
		  }

}
