package org.eclipse.gemoc.addon.xtextanimator.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.hovers.HoverAnimator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.xtext.CrossReference;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.nodemodel.ILeafNode;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.ILocationInFileProvider;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.model.XtextDocument;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;
import org.eclipse.xtext.util.ITextRegion;
import org.eclipse.xtext.util.Pair;
import org.eclipse.xtext.util.Tuples;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

import com.google.inject.Inject;

/**
 * A custom mouse listener to create hovers
 * when double clicking a variable declaration.
 * Removing the hover is done thanks to its "close" button.
 * The mouse listener is provided to the HoverMouseLinker.
 * 
 * TODO :
 *     - the findObject() method.
 *     At the moment, we can only double click on a declaration
 *     to have a hover. It would be great to have a hover no matter
 *     where the variable is used.
 */
public class XtextAnimationMouseListener implements MouseListener {

	private List<EObject> associatedObjects;
	private EObject objectFound;
	private List<HoverAnimator> hoverManagers = new ArrayList<>();
	
	@Inject
    private EObjectAtOffsetHelper eObjectAtOffsetHelper;
	@Inject
	private ILocationInFileProvider locationInFileProvider;
	
	public XtextAnimationMouseListener(List<EObject> associatedObjects, List<HoverAnimator> hoverManagers) {
		this.hoverManagers = hoverManagers;
		this.associatedObjects = associatedObjects;
	}
	
	@Override
	public void mouseDoubleClick(MouseEvent e) {
		retrieveObjectClicked();
		displayHover(e);
	}

	@Override
	public void mouseDown(MouseEvent e) {
		//TODO for more interesting actions
		//System.out.println("mouse down");
	}

	@Override
	public void mouseUp(MouseEvent e) {
		//TODO for more interesting actions
		//System.out.println("MOUSE UP");
	}

	/**
	 * Uses the Display.getDefault().syncExec in order to retrieve the
	 * Active Xtext Editor, needed to get the position of an object
	 */
	private void retrieveObjectClicked() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
		    public void run() {
				XtextEditor editor = EditorUtils.getActiveXtextEditor();
				XtextDocument document = (XtextDocument) editor.getDocument();
				ITextSelection selection = (ITextSelection) editor.getSelectionProvider().getSelection();
				findObjectAtOffset(selection, document);
			}
		});
	}
	
	/*
	 * Retrieves the correct hover and executes its display method
	 */
	private void displayHover(MouseEvent e) {
		HoverAnimator managerFound = retrieveAssociatedManager();

		if(managerFound != null) {
			boolean display = managerFound.getShell().isDisposed() || (!managerFound.getShell().isVisible());
			if(display) {
				Point location = new Point(e.x,e.y);
				managerFound.displayHover(location);
			} 
		} 
	}
	
/**
 * Finds the associated object corresponding to the selected region to find the correct manager
 * that should be triggered
 * If no object is found : there was no hover defined for the clicked content
 * @param selection is the clicked text
 * @param document is the current editor's document
 * @return the correct associated object or null if none
 * 
 * How it works : it compares the selection's offset to the nodes' offsets.
 * As nodes/concepts can have overlapping offsets (In Lingua Franca, Model contains an Input)
 * we retrieve the smallest node contained.
 * This is not an elegant solution.
 * I think it should be possible to retrieve the correct clicked object by exploiting
 * the getXtextElementAt() method (at the end of this class)
 */
	private void findObjectAtOffset(ITextSelection selection, XtextDocument document) {
		EObject associatedObject = null;
		int smallestNode = document.getLength();
		int offset = selection.getOffset();
		for(EObject object : associatedObjects) {
			INode node = NodeModelUtils.getNode(object);	
			int nodeLength = node.getText().length();
			if(node.getOffset() <= offset && offset <= node.getEndOffset()) {
				if(nodeLength < smallestNode) {
					associatedObject = object;
					smallestNode = nodeLength;
				}
			}
		}
		objectFound = associatedObject;
	}
	
	private HoverAnimator retrieveAssociatedManager() {
		if(objectFound!=null) {
			return getManagerByObject(objectFound);
		}
		return null;
	}
	
	private HoverAnimator getManagerByObject(EObject associatedObject) {
		for(HoverAnimator manager : hoverManagers) {
			if(associatedObject.equals(manager.associatedObject)) {
				return manager;
			}
		}
		return null;
	}
	
	//TODO : wesh ?
//	if (nodeForFeature.getTotalOffset() <= node.getTotalOffset()
//			&& nodeForFeature.getTotalEndOffset() >= node.getTotalEndOffset())
//		return (EObject) listValue.get(currentIndex);
	
	
	//idée : retrieve les nodes sur la ligne ? chelou idk if possiible
//TODO notes :
	//GrammarUtil si on lui donne un grammar element : on peut retrieve la grammar
	//donc le nom du langage ?
	//	EObject referenceOwner = NodeModelUtils.findActualSemanticObjectFor(node);
	//	EReference crossReference = GrammarUtil.getReference((CrossReference) node.getGrammarElement(),referenceOwner.eClass());
//	List<INode> nodesForFeature = NodeModelUtils.findNodesForFeature(referenceOwner, crossReference);

	//TODO : make this work
	private Object findObject(XtextResource resource, XtextDocument document, int offset) {
		return document.tryReadOnly(new IUnitOfWork<Object, XtextResource>() {
			@Override
			public Object exec(XtextResource state) throws Exception {
				Pair<EObject, IRegion> element = getXtextElementAt(state, offset);
				if (element != null && element.getFirst() != null) {
					return element.getFirst();
				}
				return null;
			}
		});
	}
	
	/* The following method is highly inspired (volée wsh) by :
	 * public abstract class AbstractEObjectHover extends AbstractHover implements IEObjectHover 
	 * @author Christoph Kulla - Initial contribution and API
	 * Call this method only from within an IUnitOfWork
	 */
	protected Pair<EObject, IRegion> getXtextElementAt(XtextResource resource, final int offset) {
		// check for cross reference
		EObject crossLinkedEObject = eObjectAtOffsetHelper.resolveCrossReferencedElementAt(resource, offset);
		if (crossLinkedEObject != null) {
			if (!crossLinkedEObject.eIsProxy()) {
				IParseResult parseResult = resource.getParseResult();
				if (parseResult != null) {
					ILeafNode leafNode = NodeModelUtils.findLeafNodeAtOffset(parseResult.getRootNode(), offset);
					if(leafNode != null && leafNode.isHidden() && leafNode.getOffset() == offset) {
						leafNode = NodeModelUtils.findLeafNodeAtOffset(parseResult.getRootNode(), offset - 1);
					}
					if (leafNode != null) {
						ITextRegion leafRegion = leafNode.getTextRegion();
						return Tuples.create(crossLinkedEObject, (IRegion) new Region(leafRegion.getOffset(), leafRegion.getLength()));
					}
				}
			}
		} else {
			EObject o = eObjectAtOffsetHelper.resolveElementAt(resource, offset);
			if (o != null) {
				ITextRegion region = locationInFileProvider.getSignificantTextRegion(o);
				final IRegion region2 = new Region(region.getOffset(), region.getLength());
				if (TextUtilities.overlaps(region2, new Region(offset, 0)))
					return Tuples.create(o, region2);
			}
		}
		return null;
	}

}
