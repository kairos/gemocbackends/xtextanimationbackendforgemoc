package org.eclipse.gemoc.addon.xtextanimator.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.utils.DataRetriever;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;

/*
 * Mother of all animations.
 * Its methods are to be called by the XtextAnimatorBehaviorManager
 */
public abstract class XtextAnimator {

	public EObject associatedObject; //The object we want to animate
	public GemocClockEntity clockEntity; //The object's clock, ticks if the object has been changed
	public IExecutionEngine<?> engine; //the type speaks for itself
	public boolean stepExecuted; //Indicates if the animation should happen when the step is executed or before
	public DataRetriever dataRetriever; //Allows to retrieve the current values of an object
	
	public List<EObject> auxObjects;
	public List<GemocClockEntity> auxClocks;
	
	/*
	 * To create an animation, we have to provide the object concerned by the animation,
	 * its clock entity to check when the object is updated, and the execution engine, used
	 * to create the DataRetriever.
	 * As some objects are updated before a step is executed, we need to indicate when the
	 * animation is to be executed
	 */
	public XtextAnimator(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e, boolean stepExecuted) {
		this.associatedObject = associatedObject;
		this.clockEntity = clockEntity;
		this.engine = e;
		this.stepExecuted = stepExecuted;
		this.dataRetriever = new DataRetriever(e);
		this.auxClocks = new ArrayList<>();
		this.auxObjects = new ArrayList<>();
	}
	
	/**
	 * To call when the animation needs to be updated
	 */
	abstract public void updates(); 
	
	/**
	 * When the animation needs to be cleaned
	 */
	abstract public void cleanUp();
	
	/**
	 * Only used by those who need to have multiple clocks / multiple rtds
	 * Currently, only the ChartAnimator overrides it
	 */
	public void checkAuxClocks(GemocClockEntity clockEntity){}

	public abstract void previousStepClear();

}
