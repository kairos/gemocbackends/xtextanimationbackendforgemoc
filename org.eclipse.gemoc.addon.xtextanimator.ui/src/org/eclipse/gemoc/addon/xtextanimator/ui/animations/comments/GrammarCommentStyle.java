package org.eclipse.gemoc.addon.xtextanimator.ui.animations.comments;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.utils.GrammarFinder;
import org.eclipse.xtext.AbstractRule;
import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.impl.KeywordImpl;
import org.eclipse.xtext.impl.UntilTokenImpl;

/**
 * The following class retrieves the way comments are implemented in a language,
 * in order to put a String in the correct format.
 * Implementing comments in Xtext is done by overriding the corresponding terminals.
 * 
 * In this class :
 * * ML Stands for MULTI LINE 
 * * SL Stands for SINGLE LINE -> not used at the moment
 * TODO : ask the designer if they prefer ML or SL symbols
 * TODO : let the designer choose their symbols ?
 */
public class GrammarCommentStyle {

	//THE TERMINALS AND THEIR IMPLEMENTATION PROVIDED BY org.eclipse.xtext.common.Terminals
	private static final String ML_COMMENT = "ML_COMMENT";
	private static final String SL_COMMENT = "SL_COMMENT";
	private static final String DEFAULT_START_SYMBOL = "/*";
	private static final String DEFAULT_END_SYMBOL = "*/";
	private static final String DEFAULT_SL_SYMBOL = "//";

	//FOR MULTILINE KEYWORDS : THERE IS A START Symbol (KeywordImpl) AND AN END SYMBOL (UntilImpl)
	//FOR SINGLE LINE KEYWORDS, THERE IS ONLY A START SYMBOL
	private String ml_start;
	private String ml_end;
	private String sl_start;
	
	private EList<AbstractRule> rules;
	
	//TODO : THE FOLLOWING SHOULD BE PROVIDED THROUGH RUNSTAR
	public static String languageId;

	public GrammarCommentStyle() {
		setDefaultCommentSymbols(); // We initialize the different symbols with the default ones

		IGrammarAccess gAccess = GrammarFinder.retrieveGrammar(languageId);
		if(gAccess == null) {
			return;
		}
		Grammar grammar = GrammarFinder.retrieveGrammar(languageId).getGrammar();
		rules = grammar.getRules(); // We retrieve all the rules in the grammar in order to find the rules of interest
		setLanguageCommentSymbols(); // We then try to retrieve the grammar's symbols
	}
	
	/*
	 * Writes a string in the correct multi line style
	 */
	public String correctMLComment(String comment) {
		return ml_start + comment + ml_end;
	}
	
	/*
	 * Writes a string in the correct single line style
	 */
	public String correctSLComment(String comment) {
		return sl_start + comment;
	}
	
	/*
	 * Sets the default commenting style, as defined in org.eclipse.xtext.common.Terminals
	 */
	private void setDefaultCommentSymbols() {
		setDefaultMLCommentSymbols();
		setDefaultSLCommentSymbols();
	}
	
	private void setDefaultMLCommentSymbols() {
		this.ml_start = DEFAULT_START_SYMBOL;
		this.ml_end = DEFAULT_END_SYMBOL;
	}
	
	private void setDefaultSLCommentSymbols() {
		this.sl_start = DEFAULT_SL_SYMBOL;
	}
	
	/*
	 * Retrieves the language commenting style
	 */
	private void setLanguageCommentSymbols() {
		setMLSymbols();
		setSLSymbols();
	}
	
	/**
	 * Sets the multiple line comment symbols by retrieving them from the grammar
	 * A multiline comment has 2 different symbols (start & end), thus we have
	 * to check that we properly retrieved both.
	 * For comments : 
	 * * A starting symbol is a Keyword
	 * * An ending symbol is a UntilTolken
	 */
	private void setMLSymbols() {
		boolean startIsSet = false;
		boolean endIsSet = false;
		for(AbstractRule rule : rules) {
			if(rule.getName().equals(ML_COMMENT)) { //If we find a rule that concerns multiline comments
				EList<EObject> ruleKeywords = rule.getAlternatives().eContents(); // we check its keywords : The keywords' implementation are in AlternativesImpl
				for(EObject kw : ruleKeywords) {
					if(startIsSet && endIsSet) {
						return;
					}
					else if(!startIsSet && (kw instanceof KeywordImpl)){ // if we find a starting symbol
						ml_start = ((KeywordImpl) kw).getValue();
						startIsSet = true;
					}
					else if(!endIsSet && (kw instanceof UntilTokenImpl)) { //if we find an ending symbol
						UntilTokenImpl castedkw = (UntilTokenImpl) kw;
						KeywordImpl terminal = (KeywordImpl) castedkw.getTerminal();
						ml_end = terminal.getValue();
					}
				}
			}
		}
		if(!startIsSet || !endIsSet) { // if we are missing one for who knows what reason, we use the default commenting style
			setDefaultMLCommentSymbols();
		}
	}
	
	/*
	 * Sets the single line symbol : we can have many different symbols but we only retrieve one
	 * Watch out : if single line comment, the entire line is commented (no end symbol)
	 */
	private void setSLSymbols() {
		for(AbstractRule rule : rules) {
			if(rule.getName().equals(SL_COMMENT)) { //If we find a rule that concerns single line comments
				EList<EObject> ruleKeywords = rule.getAlternatives().eContents(); // we check its keywords : The keywords' implementation are in AlternativesImpl
				for(EObject kw : ruleKeywords) {
					if(kw instanceof KeywordImpl){
						sl_start = ((KeywordImpl) kw).getValue();
						return;
					}
				}
			}
		}
	}

}
