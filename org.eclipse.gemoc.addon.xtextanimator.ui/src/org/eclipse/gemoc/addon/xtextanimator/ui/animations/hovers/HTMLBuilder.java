package org.eclipse.gemoc.addon.xtextanimator.ui.animations.hovers;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor;
import org.eclipse.swt.graphics.Color;

public class HTMLBuilder {
	
	private String fontFace = "georgia";
	private int fontSize = 12;
	private Color rgb = new Color(0,0,0);
	FontStyle fontStyle = FontStyle.NONE;
	
	private static final String quote = "\"";
	private static final String E_FONT = "</font>";
	private static final String E_P = "</p>";
	private static final String S_BOLD = "<b>";
	private static final String E_BOLD = "</b>";
	private static final String S_ITALIC = "<i>";
	private static final String E_ITALIC = "</i>";
//	private static final String S_UNDER = "<u>";
//	private static final String E_UNDER = "</u>";
	
	private String prefix = "";
	private String suffix = "";
	private String fullHTMLprefix;
	private String fullHTMLsuffix;
	
	public HTMLBuilder(String fontFace, int fontSize, RColor rgb, FontStyle fontStyle){
		if(fontFace != null) {
			this.fontFace = fontFace;
		}
		if(fontSize>0) {
			this.fontSize = fontSize;
		}
		if(rgb != null) {
			this.rgb = new Color(rgb.getRed(), rgb.getGreen(), rgb.getBlue());
		}
		this.fontStyle = fontStyle;
		this.fullHTMLprefix = "<html>";
		this.fullHTMLsuffix = "</html>";
		buildPrefixAndSuffix();
	}
	
	public String inHTML(String toCustom) {
		return fullHTMLprefix + prefix + toCustom + suffix + fullHTMLsuffix;
	}
	
	private void buildPrefixAndSuffix() {
		fullHTMLprefix += "<font face=" + quote + fontFace + quote
							+ " size=" + quote + fontSize + quote
							+ ">"
							+ "<p style=" + quote + toStringColor(rgb) + quote + ">";
		fullHTMLsuffix = E_P + E_FONT + fullHTMLsuffix;
		switch(fontStyle) {
			case ITALIC:
				fullHTMLprefix += S_ITALIC;
				fullHTMLsuffix = E_ITALIC + fullHTMLsuffix;
				break;
			case BOLD:
				fullHTMLprefix += S_BOLD;
				fullHTMLsuffix = E_BOLD + fullHTMLsuffix;
				break;
			default:
				break;
		}
	}

	private String toStringColor(Color rgb) {
		return "color:rgb(" + rgb.getRed() + "," + rgb.getGreen() + "," + rgb.getBlue() + ")";
	}
	
	public void setPrefix(String pref) {
		this.prefix = pref;
	}
	
	public void setSuffix(String suf) {
		this.suffix = suf;
	}
}
