package org.eclipse.gemoc.addon.xtextanimator.ui.animations.charts;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.utils.timeseries.GraphDrawer;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;

/*
 * Using the GraphDrawer from utils,
 * creates and updates a graph
 */
public class ChartAnimator extends XtextAnimator {

	private GraphDrawer drawer;
	private String yAttribute;
	private String xAttribute;
	private final String NULL_VALUE = "null";
//	public static int imageNumber = 0;
	public String imagePath;
	private EObject xAssociatedObject;
	private String xClassName;
	
	public ChartAnimator(EObject yAssociatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e, boolean stepExecuted,
						 String xClassName, ChartObject chart) {
		
		super(yAssociatedObject,clockEntity,e,stepExecuted);
		this.xClassName = xClassName;
		this.xAttribute = chart.getxAttribute();
		this.yAttribute = chart.getyAttribute();
		String graphTitle = chart.getGraphTitle();
		imagePath = chart.getFolderPath() + yAssociatedObject.hashCode() + ".png";
		this.drawer = new GraphDrawer(imagePath, chart.getXLabel(), chart.getYLabel(), graphTitle, chart.getNbValues(), chart.getStyle());
//		imageNumber++;
	}

	
	@Override
	public void updates() {
		int yValueToAdd = 0;
		int xValueToAdd = 0;
		String yValue = ""+dataRetriever.retrieveObject(associatedObject, yAttribute);
		String xValue = ""+dataRetriever.retrieveObject(xAssociatedObject, xAttribute);
		if(!yValue.equals(NULL_VALUE)) {
			yValueToAdd = Integer.parseInt(yValue);
		}
		if(!xValue.equals(NULL_VALUE)) {
			xValueToAdd = Integer.parseInt(xValue);
		}
		try {
			boolean pointAdded = drawer.addNextValue(xValueToAdd,yValueToAdd);
			if (pointAdded) drawer.drawGraph();
		} catch (IOException e) {
			System.out.println("graph draw exception : " + e);
			e.printStackTrace();
		}
	}

	@Override
	public void cleanUp() {
		try {
			Files.deleteIfExists(Paths.get(imagePath));
//			imageNumber=0;
		} catch (IOException e) {
			System.err.println("graph deleted exception : " + e);
			e.printStackTrace();
		}
	}
	
	/**
	 * Retrieves the correct associated EObject corresponding to the x value, for a given clock Entity
	 */
	@Override
	public void checkAuxClocks(GemocClockEntity clockEntity) {
		EObject ao = clockEntity.associatedObject;
		if(ao != null) {
			String aoName = ao.getClass().getName();
			if(aoName.equals(xClassName)) {
				this.xAssociatedObject = clockEntity.associatedObject;
			}	
		}
	}


	@Override
	public void previousStepClear() {
		// TODO Auto-generated method stub
		
	}
	
}
