package org.eclipse.gemoc.addon.xtextanimator.ui.properties;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.markers.MarkerAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.markers.MarkerIconType;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;

/*
 * Properties concerning markers.
 * This class creates a CustomMarker
 * The displayed information need specification about the attribute we want to observe
 */
public class MarkerProperties extends Properties {
	
	private List<String> attributesTracked;
	private MarkerType markerType = MarkerType.INFORMATION;
	
	public MarkerProperties(String className, String clock, boolean stepExecuted, List<String> attributesTracked, MarkerType markerType) {
		super(className, clock, stepExecuted);
		this.attributesTracked = attributesTracked;
		this.markerType = markerType;
	}

	@Override
	public XtextAnimator createAnimator(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e) {
		String icon;
		switch(markerType) {
			case WARNING:
				icon = MarkerIconType.WARNING.getIcon();
				break;
			case ERROR:
				icon = MarkerIconType.ERROR.getIcon();
				break;
			default:
				icon = MarkerIconType.INFO.getIcon();
				break;
		}
		return new MarkerAnimator(associatedObject,clockEntity,e, stepExecuted, attributesTracked, icon);
	}

}
