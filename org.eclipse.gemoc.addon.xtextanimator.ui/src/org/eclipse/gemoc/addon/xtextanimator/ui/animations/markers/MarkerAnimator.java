package org.eclipse.gemoc.addon.xtextanimator.ui.animations.markers;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModelExtension;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.xtext.diagnostics.Severity;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;
import org.eclipse.xtext.ui.editor.validation.XtextAnnotation;
import org.eclipse.xtext.validation.Issue;

/*
 * Creates a marker that displays text,
 * on the line of the concerned object
 */
public class MarkerAnimator extends XtextAnimator {

	private List<String> attributesOrText; //The attribute or text to display
	private String markerType; //The marker type : info, warning & error are the 3 proposed by xtext
	
	public MarkerAnimator(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e, boolean stepExecuted, List<String> attributesOrText, String markerType) {
		super(associatedObject, clockEntity, e, stepExecuted);
		this.attributesOrText = attributesOrText;
		this.markerType = markerType;
	}
	
	@Override
	public void updates() {
		createMarkers();
	}

	@Override
	public void cleanUp() {
		removeAllMarkers();
	}
	
	private void createMarkers() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
			 public void run() {
					XtextEditor editor = EditorUtils.getActiveXtextEditor();
					ISourceViewer sourceViewer = editor.getInternalSourceViewer();
					IAnnotationModel annotationModel = sourceViewer.getAnnotationModel();
					IAnnotationModelExtension extension = (IAnnotationModelExtension) annotationModel;
					createInformativeMarker(extension,editor);
				}
			});
	}
	
	public static void removeAllMarkers() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
			 public void run() {
					XtextEditor editor = EditorUtils.getActiveXtextEditor();
					ISourceViewer sourceViewer = editor.getInternalSourceViewer();
					IAnnotationModel annotationModel = sourceViewer.getAnnotationModel();
					IAnnotationModelExtension extension = (IAnnotationModelExtension) annotationModel;
					extension.removeAllAnnotations();	
			}
		});
	}
	
	private void createInformativeMarker(IAnnotationModelExtension extension, XtextEditor editor) {
		Issue.IssueImpl issue = new Issue.IssueImpl();
		INode node = NodeModelUtils.findActualNodeFor(associatedObject);
		int start = node.getOffset();
		int line = node.getStartLine();
		issue.setSyntaxError(false);
		issue.setSeverity(Severity.IGNORE);
		issue.setLineNumber(line);
		issue.setMessage(markerInformation());
		XtextAnnotation rangeIndicator = new XtextAnnotation(markerType, true, editor.getDocument(), issue, false);
		extension.modifyAnnotationPosition(rangeIndicator, new Position(start, 0)); //length set to 0 to avoid underlining
	}
	
	private String markerInformation() {
		String result = dataRetriever.retrieveMultipleValues(associatedObject, attributesOrText);
		String name = dataRetriever.getName(associatedObject);
		if(name.length() > 0 ) {
			result = name + "." + result;
		}
		return result;
	}

	@Override
	public void previousStepClear() {
		// TODO Auto-generated method stub
		
	}


	
}
