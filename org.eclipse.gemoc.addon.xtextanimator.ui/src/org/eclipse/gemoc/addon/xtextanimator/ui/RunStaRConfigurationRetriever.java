package org.eclipse.gemoc.addon.xtextanimator.ui;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.URIUtil;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.charts.ChartObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.hovers.HTMLBuilder;
import org.eclipse.gemoc.addon.xtextanimator.ui.properties.ChartProperties;
import org.eclipse.gemoc.addon.xtextanimator.ui.properties.CommentProperties;
import org.eclipse.gemoc.addon.xtextanimator.ui.properties.HighlightingProperties;
import org.eclipse.gemoc.addon.xtextanimator.ui.properties.MarkerProperties;
import org.eclipse.gemoc.addon.xtextanimator.ui.properties.Properties;
import org.eclipse.gemoc.addon.xtextanimator.ui.properties.StickyHoverProperties;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Comment;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ConstTextVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Hover;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImageVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Mark;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RTDVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Representation;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.TextVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable;
import org.eclipse.gemoc.execution.moccml.runstar.xtext.ui.internal.RunstarActivator;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.ocl.xtext.completeoclcs.ClassifierContextDeclCS;
import org.eclipse.ocl.xtext.completeoclcs.DefPropertyCS;
import org.eclipse.xtext.linking.lazy.LazyLinkingResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.osgi.framework.Bundle;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
/*
 * Reads the RunStaR configuration file and creates the 
 * animation properties
 */
public class RunStaRConfigurationRetriever {

	public static final String ALL_CLOCKS = "ALL_CLOCKS";
	public static final StepExecution DEFAULT_STEP_EXECUTION = StepExecution.AFTER;
	
	private RuntimeStateRepresentation rsr;
	private EList<Representation> representations;
	private EList<Variable> variables; 
	
	// THE FOLLOWING PROPERTIES ARE GENERAL INFORMATION PROPERTIES
	//private static final String GRAPH_FOLDER = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString() +"/iconstest";
	public static final String language_ID = "org.lflang.LF"; //TODO : retrieve...

	private HashMap<String, List<Properties>> configuration; //contains all the properties associated to a language concept

	//these are common parameters needed
	private TextVar representedTextVar;
	private String conceptName;
	private String clockTrigger;
	private boolean stepExecuted;
	private List<String> attributesOrText;
	private IExecutionEngine<?> ee;
	
	@Inject
	private Provider<XtextResourceSet> resourceSetProvider;
	private String prefix;
	private String suffix;
	
	public RunStaRConfigurationRetriever(IExecutionEngine<?> engine) {
		representedTextVar = null;
		conceptName = "";
		clockTrigger = "";
		stepExecuted = true;
		attributesOrText = new ArrayList<>();
		
		this.ee=engine;
		this.rsr = getRunStaRModel(engine);
		this.representations = rsr.getRepresentations();
		this.variables = rsr.getVariables();
		this.configuration = new HashMap<>();
		createRepresentations(); //creates all the properties objects and stores them in the hashmap configuration
	}
	
	public HashMap<String, List<Properties>> getConfiguration() {
		return this.configuration;
	}
	
	/*
	 * Method to retrieve the runstar configuration file
	 */
	public RuntimeStateRepresentation getRunStaRModel(IExecutionEngine<?> engine) {
		RuntimeStateRepresentation model = null;
		try {
			String path = engine.getExecutionContext().getLanguageDefinitionExtension().getXDSMLFilePath();
//			System.out.println("LANGUAGE" + engine.getExecutionContext().getLanguageDefinitionExtension());
			String bundleName = path.substring(0, path.indexOf('/', 1)+1);
			Bundle bundle = Platform.getBundle(bundleName.replaceAll("/", ""));
			URL url = FileLocator.find(bundle, new Path(path.replaceFirst(bundleName, "")), null);
			File f = URIUtil.toFile(URIUtil.toURI(FileLocator.toFileURL(url)));
			String runstarPath = "platform:/plugin"+Files.lines(f.toPath()).filter(l -> l.contains("="))
					.collect(Collectors.toMap(k -> k.split("\\s*=\\s*")[0], v -> v.split("\\s*=\\s*")[1]))
					.get("runstar");
						
			Injector injector = RunstarActivator.getInstance().getInjector(RunstarActivator.ORG_ECLIPSE_GEMOC_EXECUTION_MOCCML_RUNSTAR_XTEXT_RUNSTAR);
			injector.injectMembers(this);
		
			XtextResourceSet resourceSet = resourceSetProvider.get();
			
			resourceSet.getURIConverter().getURIMap().putAll(EcorePlugin.computePlatformURIMap(true));
			Resource resource = resourceSet.getResource(URI.createURI(runstarPath), true);
			model = (RuntimeStateRepresentation) resource.getContents().get(0);
		} catch (Exception e) {
			System.err.println("Unable to load the runstar model");
		}
		return model;
	}
	
	/*
	 * Creates all the properties objects and then stores them in the hashmap configuration
	 */
	private void createRepresentations() {
		this.conceptName = "";
		this.clockTrigger = "";
		this.prefix = "";
		this.suffix = "";
		this.rsr = null;
		for(Representation rpz : representations) {
			Properties property = switchOnRepresentationType(rpz);
			if(property != null) {
				addPropertyToConfiguration(property);
			}
		}
		for(Variable var : variables) {
			Properties property = switchOnVarType(var);
			if(property != null) {
				addPropertyToConfiguration(property);
			}
		}
	}
	
	/*
	 * Stores a property in the hashmap configuration
	 */
	private void addPropertyToConfiguration(Properties property) {
//		si className est null, alors prendre tous les concepts qui ont le clock trigger et les mettre dans la map
		
		
		String concept = property.className;
		if(configuration.containsKey(concept)) {
			List<Properties> properties = configuration.get(concept);
			properties.add(property);
			configuration.replace(concept, properties);
		} else {
			List<Properties> properties = new ArrayList<>();
			properties.add(property);
			configuration.put(concept, properties);
		}
	}
	
	/*
	 * Given a Representation, creates the corresponding property
	 */
	private Properties switchOnRepresentationType(Representation rpz) {
		if(rpz instanceof Hover) {
			return createHover(rpz);
		}
		else if (rpz instanceof Mark) {
			return createMarker(rpz);
		}
		else if (rpz instanceof Paint) {
			return createHighlight(rpz);
		}
		else if (rpz instanceof Comment) {
			return createCommenter(rpz);
		}
		else {
			return null;
		}
	}
	
	/* 
	 * Given a Variable, creates the corresponding property
	 */
	private Properties switchOnVarType(Variable var) {
		if(var instanceof ChartVar) {
			return createChart((ChartVar) var);
		}
		else return null;
	}
	
	private CommentProperties createCommenter(Representation rpz) {
		setCommonVariables(rpz);
		return new CommentProperties(conceptName, clockTrigger, stepExecuted, attributesOrText);
	}
	
	private MarkerProperties createMarker(Representation rpz) {
		setCommonVariables(rpz);
		Mark marker = (Mark) rpz;
		MarkerType type = marker.getMarkerType(); //a marker has a specific parameter to retrieve
		return new MarkerProperties(conceptName, clockTrigger, stepExecuted, attributesOrText, type);
	}

	/*
	 * Highlights are not quite like the other animations. It does not have a getText() method,
	 * which is why we retrieve its parameters the following way
	 */
	private HighlightingProperties createHighlight(Representation rpz) {
		
		
		Paint paint = (Paint) rpz;
		if(paint.getOnConcept() != null) {
			conceptName = conceptName(paint.getOnConcept());
		}
		String stepExecution = strategyStepExecution(paint.getOwnedUpdateStrategy());
		stepExecuted = stepExecution != StepExecution.BEFORE.getLiteral();
		clockTrigger = strategyClockTrigger(paint.getOwnedUpdateStrategy());
		RColor color = paint.getColor();
		boolean background = paint.getType() == PaintType.BACKGROUND;
		return new HighlightingProperties(conceptName, clockTrigger, stepExecuted, color, background, paint.getPersistent() == RBoolean.TRUE);
	}
	
	/*
	 * Hovers are a bit more complicated : you can display a text, an image, or both
	 */
	private StickyHoverProperties createHover(Representation rpz) {
		setCommonVariables(rpz);
		ImageVar representedImage = rpz.getImage();
		boolean displayText = true;
		String imagePath = "";
		if(representedImage != null) { //we check if the retrieved image is null, in order to know if we have to display one or not 
			imagePath = representedImage.getPath();
			displayText = (representedTextVar != null); //if there is an image to display, we must know if there is also text to display
			setHoverText(representedImage);
		}
		
		String fontFace = "georgia";
		int fontSize = 12;
		RColor rgb = null;
		FontStyle fontStyle = FontStyle.NONE;
		prefix = "";
		suffix = "";
		if(representedTextVar instanceof DecoratedTextVar) {
			DecoratedTextVar var = (DecoratedTextVar) representedTextVar;
			rgb = var.getColor();
			fontStyle = var.getFontStyle();
			fontSize = var.getFontSize();
			fontFace = var.getFont();
			if(var.getPrefix() != null) {
				prefix = var.getPrefix();
			}
			if(var.getSuffix() != null) {
				suffix = var.getSuffix();
			}
		}
		boolean fromStart = (((Hover) rpz).getFromStart() == RBoolean.TRUE); //when the hover should be displayed
		HTMLBuilder htmlBuilder = new HTMLBuilder(fontFace, fontSize, rgb, fontStyle); //the object that styles the hover text
		htmlBuilder.setPrefix(prefix);
		htmlBuilder.setSuffix(suffix);
		return new StickyHoverProperties(conceptName, clockTrigger, stepExecuted, attributesOrText, imagePath, htmlBuilder, fromStart, displayText);
	}
	
	private void setHoverText(ImageVar representedImage) {
		if(representedTextVar == null) {
			if(representedImage instanceof ChartVar) {
				ChartVar chart = (ChartVar) representedImage;
				setChartVars(chart);
			}
			else {
				setImageVars(representedImage);
			}
		}
	}
	
	private ChartProperties createChart(ChartVar var) {
		setChartVars(var);
		ChartObject chart = new ChartObject(var);
		String xClassName = conceptName(var.getXrtd());
		return new ChartProperties(conceptName, clockTrigger, stepExecuted, chart, xClassName);
	}
	
	private void setChartVars(ChartVar var) {
		RTDVar rtd = var.getYrtds().get(0); //TODO for the future : adapt to display multi y rtds at the same time
		conceptName = conceptName(rtd);
		clockTrigger = strategyTrigger(rtd);
		String stepExecution = strategyProcess(rtd);
		stepExecuted = stepExecution != StepExecution.BEFORE.getLiteral();
	}
	
	private void setImageVars(ImageVar image) {
		conceptName = conceptName(image.getStrategy().getOnConcept());
		clockTrigger = strategyTrigger(image);
		String stepExecution = strategyProcess(image);
		stepExecuted = stepExecution != StepExecution.BEFORE.getLiteral();
	}
	
/////////////////////////////// USEFUL METHODS	
	
	/*
	 * As you can decorate a decorated text, we need the following method to apply all the recursive decorations
	 */
	private void flattenDecoration(DecoratedTextVar decoration) {
		if(decoration.getDecorated() instanceof DecoratedTextVar) {
			DecoratedTextVar inside = (DecoratedTextVar)decoration.getDecorated();
			flattenDecoration(inside);
			if(decoration.getPrefix() == null) decoration.setPrefix(inside.getPrefix());
			if(decoration.getSuffix() == null) decoration.setSuffix(inside.getSuffix());
			if(decoration.getFont() == null) decoration.setFont(inside.getFont());
			if(decoration.getFontStyle().equals(FontStyle.NONE)) decoration.setFontStyle(inside.getFontStyle());
			if(decoration.getFontSize() <= 0) decoration.setFontSize(inside.getFontSize());
			if(decoration.getColor() == null) decoration.setColor(inside.getColor());
			decoration.setDecorated(inside.getDecorated());
		}
	}
	
	/*
	 * Sets variable according to the representedTextVar,
	 * if there is one
	 */
	private void setCommonVariables(Representation rpz) {
			representedTextVar = rpz.getText();
			conceptName = "";
			clockTrigger = "";
			stepExecuted = true;
			attributesOrText = new ArrayList<>();
 			if(representedTextVar != null) {
 				setTextVar(representedTextVar);
			}
		}
	
	/*
	 * Sets variable according to the representedTextVar
	 */
	private void setTextVar(TextVar textVar) {
		representedTextVar = textVar;
		attributesOrText = attributeOrConst(representedTextVar);
		conceptName = conceptName(representedTextVar);
		clockTrigger = strategyTrigger(representedTextVar);
		String stepExecution = strategyProcess(representedTextVar);
		stepExecuted = (stepExecution != (StepExecution.BEFORE.getLiteral()));
	}
	
	/*
	 * Returns a list of text. Can be a list of attributes
	 */
	private List<String> attributeOrConst(TextVar var) {
		List<String> attributeList = new ArrayList<>();
		if(var instanceof DecoratedTextVar) {
			DecoratedTextVar deco = (DecoratedTextVar) var;
			flattenDecoration(deco);
			var = deco.getDecorated();
		}
		if(var instanceof RTDVar) {
			attributeList.addAll(((RTDVar) var).getAttributes());
		}
		else if(var instanceof ConstTextVar) {
			String text = ((ConstTextVar) var).getValue();
			attributeList.add(text);
		}
		return attributeList;
	}
	
	/*
	 * For a given Variable, a language concept, returns its name
	 */
	private String conceptName(Variable var) {
		EClass clazz = null;
		if(var instanceof DecoratedTextVar) {
			DecoratedTextVar deco = (DecoratedTextVar) var;
			flattenDecoration(deco);
			var = deco.getDecorated();
		}
		clazz = (EClass) var.getStrategy().getOnConcept();
		Resource resInEngine = ee.getExecutionContext().getResourceModel();
		EPackage lfep = (EPackage) resInEngine.getContents().get(0).eClass().eContainer();
		Class<? extends EObject> theClassYoureLookingFor = lfep.getEFactoryInstance().create(clazz).getClass();
		String theQualifiedName = theClassYoureLookingFor.getCanonicalName();
		return theQualifiedName;
	}
	
	/*
	 * For a given EObject, a language concept, returns its name
	 */
	private String conceptName(EObject concept) {
		Resource resInEngine = ee.getExecutionContext().getResourceModel();
		ResourceSet rset = resInEngine.getResourceSet();
		
		Class<? extends EObject> theClassYoureLookingFor = getClassFromConcept(concept, rset);
		return (theClassYoureLookingFor==null)?"ConceptNotFoundInPackages":theClassYoureLookingFor.getCanonicalName();
	}

	private Class<? extends EObject> getClassFromConcept(EObject concept, ResourceSet rset) {
		Exception lastException =null;
		for (Resource res: rset.getResources().stream().filter(r -> r instanceof LazyLinkingResource).collect(Collectors.toList())) {
			try{
				EPackage lfep = (EPackage) res.getContents().get(0).eClass().eContainer();
				return (Class<? extends EObject>) lfep.getEClassifier(((EClass)concept).getName()).getInstanceClass();//.create((EClass)concept).getClass();
			}catch(Exception e) {
				lastException = e;
				continue;
			}
		}
		System.err.println(lastException);
		return null;
	}
	
	/*
	 * For a given variable, retrieves if the animation should execute BEFORE or
	 * AFTER a certain clock."all clocks" if the animation should execute for any clock
	 */
	private String strategyProcess(Variable var) {
		if(var instanceof DecoratedTextVar) {
			DecoratedTextVar deco = (DecoratedTextVar) var;
			flattenDecoration(deco);
			var = deco.getDecorated();
		}
		if(var.getStrategy()==null) return RunStaRConfigurationRetriever.ALL_CLOCKS;
		return strategyStepExecution(var.getStrategy().getOwnedUpdateStrategy());
	}
	
	/*
	 * For a given strategy, retrieves if the animation should execute BEFORE or
	 * AFTER a certain clock."all clocks" if the animation should execute for any clock
	 */
	private String strategyStepExecution(RUpdateStrategy strategy) {
		if(strategy == null) return RunStaRConfigurationRetriever.ALL_CLOCKS;
		return ""+strategy.getStepExecution();
	}
	
	/*
	 * For a given variable, retrieves the clock name or "all clocks" if the animation
	 * should execute for any clock
	 */
	private String strategyTrigger(Variable var) {
		if(var instanceof DecoratedTextVar) {
			DecoratedTextVar deco = (DecoratedTextVar) var;
			flattenDecoration(deco);
			var = deco.getDecorated();
		}
		return strategyClockTrigger(var.getStrategy().getOwnedUpdateStrategy());
	}

	/*
	 * For a given strategy, retrieves the clock name or "all clocks" if the animation
	 * should execute for any clock
	 */
	private String strategyClockTrigger(RUpdateStrategy strategy) {
		if(strategy == null) return RunStaRConfigurationRetriever.ALL_CLOCKS;
		if(strategy.getEvents().isEmpty()) return RunStaRConfigurationRetriever.ALL_CLOCKS;
		DefPropertyCS strat = strategy.getEvents().get(0);
		String trigger = strat.getName();
		if(trigger == null) trigger = RunStaRConfigurationRetriever.ALL_CLOCKS;
		return trigger;
	}
	
	/**
	 * For a given strategy, retrieves the definition context of the event or null
	 * @strategy : the strategy as defined in runstar
	 * @return the context of definition of the event or null if the event is not defined
	 */
	private String strategyEventContext(RUpdateStrategy strategy) {
		if(strategy == null) return null;
		if(strategy.getEvents().isEmpty()) return null;
		DefPropertyCS prop = strategy.getEvents().get(0);
		EObject clazz = ((ClassifierContextDeclCS)prop.eContainer()).getReferredClass().getESObject();
//		EcoreUtil.resolveAll(clazz.eResource().getResourceSet());
//		EcoreUtil.resolveAll(clazz);
		
		return conceptName(clazz);
	}
	
}
