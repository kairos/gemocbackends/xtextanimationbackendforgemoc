package org.eclipse.gemoc.addon.xtextanimator.ui.properties;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.comments.CommenterAnimator;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;

public class CommentProperties extends Properties {

	public List<String> attributesTracked;
	
	public CommentProperties(String className, String clockTrigger, boolean stepExecuted, List<String> attributesTracked) {
		super(className, clockTrigger, stepExecuted);
		this.attributesTracked = attributesTracked;
	}

	@Override
	public XtextAnimator createAnimator(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e) {
		CommenterAnimator commenter = new CommenterAnimator(associatedObject, clockEntity, e, stepExecuted, attributesTracked);
		return commenter;
	}

}
