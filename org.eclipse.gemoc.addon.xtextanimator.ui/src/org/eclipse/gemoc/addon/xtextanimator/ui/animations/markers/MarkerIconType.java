package org.eclipse.gemoc.addon.xtextanimator.ui.animations.markers;

//Fully customizing the marker icon might be possible with a little bit of hacking. ctrl+click :
// https://www.eclipse.org/forums/index.php/m/1843176/#msg_1843176

public enum MarkerIconType {
	
	INFO("org.eclipse.xtext.ui.editor.info"), 
	WARNING("org.eclipse.xtext.ui.editor.warning"),
	ERROR("org.eclipse.xtext.ui.editor.error");

	private String icon;
	
	MarkerIconType(String string) {
		this.icon = string;
	}
	
	public String getIcon() {
		return icon;
	}
}
