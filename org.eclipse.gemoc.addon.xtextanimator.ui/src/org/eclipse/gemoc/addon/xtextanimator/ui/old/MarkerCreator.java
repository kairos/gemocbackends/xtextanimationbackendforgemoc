package org.eclipse.gemoc.addon.xtextanimator.ui.old;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.comments.GrammarCommentStyle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;

public class MarkerCreator {

public MarkerCreator() {
	
}
	public void handleMarkers(GrammarCommentStyle commentStyle) {
		Display.getDefault().syncExec(new Runnable() {
			@Override
		    public void run() {
				XtextEditor editor = EditorUtils.getActiveXtextEditor();
				IXtextDocument document = editor.getDocument();
				IResource resource = editor.getResource();
				IWorkspace workspace = resource.getWorkspace();
				try {
					IMarker marker = resource.createMarker(IMarker.MARKER);
					marker.setAttribute(IMarker.TRANSIENT, true);
					workspace.getRoot().createMarker(IMarker.MARKER);
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					IMarker marker = resource.createMarker(IMarker.MARKER);
					System.out.println("I tried creating a marker");
					marker.setAttribute(IMarker.TRANSIENT, true);
					workspace.getRoot().createMarker(IMarker.MARKER);
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
	
}
