package org.eclipse.gemoc.addon.xtextanimator.ui.properties;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.highlights.HighlighterAnimator;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.swt.graphics.Color;

/*
 * Properties concerning highlights.
 * The associated object will be highlighted when changed
 * The highlight color must be specified, in RGB format (3 integers)
 */
public class HighlightingProperties extends Properties {

	private Color highlightColor;
	private boolean background;
	private boolean isPersistent;
	
	public HighlightingProperties(String className, String clockTrigger, boolean stepExecuted,
								  RColor color, boolean background, boolean isPersistent) {
		super(className, clockTrigger, stepExecuted);
		if(color != null) {
			this.highlightColor = new Color(color.getRed(),color.getGreen(),color.getBlue());
		}
		else {
			this.highlightColor = null;
		}
		this.background = background;
		this.isPersistent = isPersistent;
	}

	@Override
	public XtextAnimator createAnimator(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e) {
		return createHighlighter(associatedObject, clockEntity, e);
	}
	
	private HighlighterAnimator createHighlighter(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e) {
		HighlighterAnimator highlighter = new HighlighterAnimator(associatedObject, clockEntity, e, stepExecuted, this.highlightColor, this.background, this.isPersistent);
		return highlighter;
	}

	
}
