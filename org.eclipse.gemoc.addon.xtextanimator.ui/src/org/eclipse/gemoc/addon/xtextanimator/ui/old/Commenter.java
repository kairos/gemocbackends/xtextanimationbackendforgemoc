package org.eclipse.gemoc.addon.xtextanimator.ui.old;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.comments.GrammarCommentStyle;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.FindReplaceDocumentAdapter;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;

/*
 * Adds comments next to the associatedObject in the .dsl file
 * OLD COMMENTER THAT EDITS THE XTEXTDOCUMENT (INVSIVE COMMENT)
 * MAYBE IT CAN BE USED IN THE FUTURE TO HAVE 'EDITABLE' COMMENTS,
 * TO CHANGE VALUES AT RUNTIME
 * WARNING : HAS ISSUES WHEN COMBINED WITH HIGHLIGHTING
 * WRITING A COMMENT 'MOVES' THE HIGHLIGHT OFFSET
 */
public class Commenter extends XtextAnimator{
	
	private boolean commentWasAdded; //indicates if we have already written a comment or not
	private String commentAdded = ""; //the comment written
	private int commentOffset = 0; //the position of the comment
	private List<String> attributesTracked; //TODO : TRACK MULTIPLE ATTRIBUTES OF AN OBJECT
	//private String attributeTracked;
	private FindReplaceDocumentAdapter documentAdapter;
	private GrammarCommentStyle commentStyle; //Helper class, to retrieve the way comments are implemented in a language
	
	public Commenter(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e, boolean stepExecuted, String attributeTracked) {
		super(associatedObject, clockEntity, e, stepExecuted);
		commentWasAdded = false;
		this.attributesTracked = new ArrayList<String>();
		attributesTracked.add(attributeTracked);
		this.commentStyle = new GrammarCommentStyle();
	}
	
	@Override
	public void updates() {
			handleComments();	
	}
	
	@Override
	public void cleanUp() {
		cleanLastComment();	
	}
	
	private boolean oldCommentEqualsNew() {
		String newComment = newComment();
		return commentAdded.equals(newComment);
	}
	
	/**
	 * Updates comments by using the active editor
	 * Saves the editor to avoid leaving it in a
	 * "edited" state. 
	 * TODO: put editor in readOnly
	 * @param commentStyle the commenting style defined in the grammar
	 */
	public void handleComments() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
		    public void run() {
				XtextEditor editor = EditorUtils.getActiveXtextEditor();
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				IXtextDocument document = editor.getDocument();
				documentAdapter = new FindReplaceDocumentAdapter(document);			
				if (associatedObject.eContainer() != null) {
					EcoreUtil2.resolveAll(associatedObject.eContainer());
				}
				editComment(document);
				page.saveEditor(editor, false);
				editor.doSave(new NullProgressMonitor());
			}
		});
	}
	
	private void editComment(IXtextDocument document) {
		if(commentWasAdded && !oldCommentEqualsNew())  {
			removeComment(document);
		}
			addComment(document);
	}
	
	private void addComment(IXtextDocument document)  {
		INode node = NodeModelUtils.getNode(associatedObject);
		int offset = 0;
		try {
			offset = document.getLineOffset(node.getEndLine())-1;
			commentAdded = " " + newComment();
			commentOffset = offset;
			document.replace(offset,0, commentAdded);
			commentWasAdded=true;
		} catch (BadLocationException e) {
			commentAdded="";
			commentOffset=0;
			e.printStackTrace();
		}
	}
	
	/**
	 * @return the values associated to the tracked attributes of the associatedObject
	 */
	private String commentContent() {
		return dataRetriever.retrieveMultipleValues(associatedObject, attributesTracked);
	}
	
	private String newComment() {
		return commentStyle.correctMLComment(commentContent());
	}
	
	private void removeComment(IXtextDocument document) {
		try {
			commentOffset = documentAdapter.find(0, commentAdded, true, false, false, false).getOffset();
			
			document.replace(commentOffset, commentAdded.length(), "");
			commentWasAdded=false;
		} catch (BadLocationException e) {
			System.out.println("In removeComment exception ! ");
			e.printStackTrace();
		}
	}
	
	public void cleanLastComment() {
		if(commentWasAdded) {
			Display.getDefault().syncExec(new Runnable() {
				@Override
			    public void run() {
					XtextEditor editor = EditorUtils.getActiveXtextEditor();
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					IXtextDocument document = editor.getDocument();
					if(commentWasAdded) removeComment(document);
					editor.doSave(new NullProgressMonitor());
					page.saveEditor(editor, false);
				}
			});
		}
	}

	@Override
	public void previousStepClear() {
		// TODO Auto-generated method stub
		
	}

}
