package org.eclipse.gemoc.addon.xtextanimator.ui.properties;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;

/*
 * Mother of all Properties.
 * This class allows retrieval of the necessary properties for animations.
 * It can then create the corresponding XtextAnimator object, according to 
 * the defined properties.
 * One animation concerns one type of object.
 */
public abstract class Properties {

	public String className; //The class name of the tracked object
	public String clockTrigger; //Which clock trigger interests us
	public boolean stepExecuted; //if the animation should be executed after the step execution

	
	public Properties(String className, String clockTrigger, boolean stepExecuted) {
		this.className = className;
		this.clockTrigger = clockTrigger;
		this.stepExecuted = stepExecuted;
	}
	
	/**
	 * Creates the associated XtextAnimator according to the given properties
	 */
	public abstract XtextAnimator createAnimator(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e);
	
}
