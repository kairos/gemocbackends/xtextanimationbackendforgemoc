//package org.eclipse.gemoc.addon.xtextanimator.ui.old;
//
//import org.eclipse.emf.ecore.EObject;
//import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
//import org.eclipse.gemoc.addon.xtextanimator.ui.hovers.CustomHover;
//import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
//import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
//import org.eclipse.jface.action.ToolBarManager;
//import org.eclipse.jface.internal.text.html.BrowserInformationControl;
//import org.eclipse.jface.text.IInformationControl;
//import org.eclipse.jface.text.IInformationControlCreator;
//import org.eclipse.jface.text.IRegion;
//import org.eclipse.swt.SWT;
//import org.eclipse.swt.browser.Browser;
//import org.eclipse.swt.graphics.Color;
//import org.eclipse.swt.graphics.Image;
//import org.eclipse.swt.graphics.Point;
//import org.eclipse.swt.graphics.Rectangle;
//import org.eclipse.swt.layout.GridData;
//import org.eclipse.swt.layout.GridLayout;
//import org.eclipse.swt.widgets.Composite;
//import org.eclipse.swt.widgets.Display;
//import org.eclipse.swt.widgets.Event;
//import org.eclipse.swt.widgets.Label;
//import org.eclipse.swt.widgets.Listener;
//import org.eclipse.swt.widgets.Shell;
//import org.eclipse.swt.widgets.Text;
//import org.eclipse.xtext.nodemodel.INode;
//import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
//import org.eclipse.xtext.ui.editor.hover.html.IXtextBrowserInformationControl;
//import org.eclipse.xtext.ui.editor.hover.html.XtextBrowserInformationControl;
//
//public class HoverVisibilityManager extends XtextAnimator {
//
//	private IInformationControl informationControl; // is responsible of the information display
//	private Shell shell; // needed for the custom hover and its information control creation
//	private Point objectLocation;
//	private CustomHover hover;
//	private boolean isVisible = false;
//	private String attributeTracked;
//	public String textColor; 
//	public String imagePath;
//	//private HoverManagerSWT swtHover;
//	
//	public HoverVisibilityManager(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e, boolean stepExecuted, 
//								  String attributeTracked,  String textColor, String imagePath) {
//		super(associatedObject, clockEntity, e,stepExecuted );
//		this.attributeTracked = attributeTracked;
//		this.textColor = textColor;
//		this.imagePath = imagePath;
//	//	this.swtHover = new HoverManagerSWT(associatedObject, clockEntity, e, stepExecuted, attributeTracked, textColor, imagePath);
//	}
//	
//	@Override
//	public void updates() {
//		//YOU MAY WONDER WHY THE DISPLAY.GETDEFAULT().SYNCEXEC()
//		// LET ME EXPLAIN :
//		// WHEN YOU TRY TO ACCESS SWT STUFF "OUTSIDE" AN APPROPRIATE THREAD, LIKE THE FOLLOWING,
//		//YOUR TOOLS THAT AFFECT THE DISPLAY WILL RECEIVE A THREAD EXCEPTION
//		//THIS IS WHY THE COMPUTENEWINFORMATION() METHOD IS INSIDE THIS DISPLAY SYNC EXEC
//		Display.getDefault().syncExec(new Runnable() {
//			@Override
//		    public void run() {
//				computeNewInformation();
//			}
//		});
//		//swtHover.updates();
//	}
//	
//	public void setImagePath(String imagePath) {
//		this.imagePath = imagePath;
//		//this.swtHover.imagePath = imagePath;
//	}
//
//	@Override
//	public void cleanUp() {
//		this.informationControl.dispose();
//		//swtHover.cleanUp();
//	}
//	
//	//TODO SHOULD BE OK TO FUSE DISPLAY AND REMOVE IN ONE METHOD
//	public void displayHover(Point location) {
//		this.objectLocation = location;
//		computeNewInformation();
//		this.informationControl.setVisible(true);
//		this.isVisible = true;
//	//	swtSetUp();
//		//swtHover.displayBrowser(location);
//	}
//	
//	public boolean isVisible() {
//		return isVisible;
//	}
//	
//	public void hideHover() {
//		this.informationControl.setVisible(false);
//		this.isVisible = false;
//		//swtHover.hideHover();
//	}
//	
//	public void setUp() {
//		shell = Display.getCurrent().getActiveShell();
//		hover = new CustomHover(shell, engine, attributeTracked, imagePath);
//		hover.textColor = this.textColor;
//		this.informationControl = getHoverControlCreator().createInformationControl(shell);	
//		int length = 500;
//		int width = 500;
//		Point location = computeObjectLocation();
//		Rectangle area = new Rectangle(location.x, location.y, length, width);
//		//String information = hover.getHoverInfoAsHtml(associatedObject);
//		//setControlParameters(information,area,location);
//		//swtHover.setUpHover();
//	}
//	
//	public void swtSetUp() {
//		Shell shell2 = new Shell(Display.getCurrent());
//		RunStarHover hover = new RunStarHover(shell2);
//		shell2.pack();
//		shell2.open();
////		hover = new CustomHover(shell, engine, attributeTracked, imagePath);
////		hover.textColor = this.textColor;
////		String information = hover.getHoverInfoAsHtml(associatedObject);
////	    Shell shell2 = new Shell(Display.getCurrent());
//	    Label label = new Label(shell2, SWT.CENTER);
//	    label.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
////	  //  Image image = new Image(Display.getCurrent(),imagePath);
////	    
////	    label.setText(information);
////	    label.pack(); // pack() => setSize (computeSize (SWT.DEFAULT, SWT.DEFAULT, changed));
////	    shell2.pack();
////	    shell2.open();
//	    
//	    
//	  //  shell2.forceActive();
//	//    Browser browser = new Browser(shell2, SWT.DEFAULT);
//	   // this.informationControl = (IInformationControl) browser;
//	  //  browser.setBounds(50, 50, 100, 100);
//	  //  browser.setJavascriptEnabled(true);
////	    browser.setText(information);
////	    browser.setVisible(isVisible);
//	    //((XtextBrowserInformationControl) this.informationControl).setInput(label);
//	  //  label.forceFocus();
//	}
//	
//		class RunStarHover extends Composite {
//		    public CustomHover internalHover;
//			private Browser b;
//			private Shell hoverShell;
//		    public RunStarHover(Shell shell) {
//		    	super(new Shell(shell,SWT.SHELL_TRIM | SWT.H_SCROLL | SWT.V_SCROLL| SWT.BORDER | SWT.RESIZE), SWT.H_SCROLL | SWT.V_SCROLL);
//		        hoverShell = new Shell(shell);
//		        this.setLayout(new GridLayout());	        
////				final Composite toolB = new Composite(shell, SWT.BORDER | SWT.RESIZE);
//				this.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
//		        internalHover = new CustomHover(this.getShell(), engine, attributeTracked, imagePath);
////		        hoverShell.setText("Browser Example");
//		        this.setSize(350, 350);
////		        hoverShell.setSize(350, 350);
//				b = new Browser(this.getShell(), 0);
//				b.setBounds(0, 0, 350, 350);
//				b.setJavascriptEnabled(true);
//				//b.setText(internalHover.getHoverInfoAsHtml(associatedObject));
////				b.setUrl("http://google.fr/");
//				b.setVisible(true);
////		        
//				hoverShell.addListener (SWT.Resize,  new Listener () {
//				    public void handleEvent (Event e) {
//				      Rectangle rect = shell.getClientArea ();
//				      System.out.println(rect);
//				    }
//				  });
//				
////		        Text text = new Text(hoverShell, SWT.NONE);
////		        text.setBackground(hoverShell.getBackground());
////		        text.setEditable(false);
////		        text.setSize(100, 100);
//		    }
//		}
//		
//	
//	//Trough tests, it appeared that the information control needs to be refreshed to 
//	//display the new information. This works
//	public void computeNewInformation() {
//		String newInformation = hover.getHoverInfoAsHtml(associatedObject);
//		if(this.isVisible) { 
//			this.informationControl.setVisible(false);
//			this.informationControl.setInformation(newInformation);
//			this.informationControl.setVisible(true);
//		}
//		else {
//			this.informationControl.setInformation(newInformation);
//			if(objectLocation!=null) {
//				this.informationControl.setLocation(objectLocation);
//			}
//		}
//		//this.swtHover.updateInformation();
//	}
//	
//	private void setControlParameters(String information, Rectangle area,Point location) {
//		this.informationControl.setInformation(information);
//		this.informationControl.setLocation(location);
//		this.informationControl.setSize(area.height, area.width);
//	}
//	
//	private Point computeObjectLocation() {
//		INode node = NodeModelUtils.getNode(associatedObject);
//		int y = node.getStartLine();
//		return new Point(200,500);
//	}
//
//	private IInformationControlCreator getHoverControlCreator() {
//		IInformationControlCreator infoControl =  new IInformationControlCreator() {
//			@Override
//			public IInformationControl createInformationControl(Shell parent) {
//				ToolBarManager tbm = new ToolBarManager(SWT.CLOSE);
//				String font = "org.eclipse.jdt.ui.javadocfont"; 
//				IXtextBrowserInformationControl xtextInfoControl = new XtextBrowserInformationControl(parent, font, tbm);
//				return xtextInfoControl;
//			}
//		};
//		return infoControl;
//	}
	
	//RESULT OF INVESTIGATIONS ON HOW HOVERS ARE CREATED
	
	/*
	 When launching Runtime Eclipse, the XtextEditor is initialized and initializes its
	 XtextSourceViewer, which initialized its TextViewerHoverManager.
	 
	 The TextViewerHoverManager has a 
	 
	 @Override protected void computeInformation() {}, that
	 "Determines all necessary details and delegates the computation into a background thread. "
	
	 Looking into this method, we can see that we retrieve :
	 - a location (getHoverEventLocation() )
	 - a hover ( fTextViewer.getTextHover(...) )
	 - a Region ( hover.getHoverRegion(...) )
	 - a Rectangle ( area = JFaceTextUtil.computeArea(region, fTextViewer) )
	 We then retrieve "informations", depending on the hover type (ITextHoverExtension2 or ITextHoverExtension).
	 //TODO : methods
	 The next step is the call to setInformation(information, area).
	 TextViewerHoverManager extends AbstractHoverInformationControlManager that extends AbstractInformationControlManager.
	 This class is where the setInformation(..) is defined, as follows :

//////////////////////////////	 
	 * Sets the parameters of the information to be displayed. These are the information itself and
	 * the area for which the given information is valid. This so called subject area is a graphical
	 * region of the information control's subject control. This method calls <code>presentInformation()</code>
	 * to trigger the presentation of the computed information.
	 *
	 * @param information the information, or <code>null</code> if none is available
	 * @param subjectArea the subject area, or <code>null</code> if none is available
	 * @since  2.1
	 
	protected final void setInformation(Object information, Rectangle subjectArea) {
		fInformation= information;
		fSubjectArea= subjectArea;
		presentInformation();
	}
//////////////////////////////	 
 
	 We can see a call to the method presentInformation() that is as follows :
	 
//////////////////////////////	 
	 * Presents the information in the information control or hides the information
	 * control if no information should be presented. The information has previously
	 * been set using setInformation
	 * This method should only be called from overriding methods or from setInformation

	protected void presentInformation() {
		boolean hasContents= false;
		if (fInformation instanceof String)
			hasContents= !((String)fInformation).trim().isEmpty();
		else
			hasContents= (fInformation != null);

		if (fSubjectArea != null && hasContents)
			internalShowInformationControl(fSubjectArea, fInformation);
		else
			hideInformationControl();
	}
//////////////////////////////	 
 	
 	The method internalShowInformationControl(Rectangle subjectArea, Object information)
	"Opens the information control with the given information and the specified
	 subject area. It also activates the information control closer."
	 Inside, an IInformationControl is initialized. Its information, size, location are set
	 before calling at the end 
	 
	 showInformationControl(subjectArea); that
	 "Shows the information control and starts the information control closer.
	 This method may not be called by clients."

//////////////////////////////	 
	protected void showInformationControl(Rectangle subjectArea) {
		fInformationControl.setVisible(true);
		if (fInformationControl == null)
			return; // could already be disposed if setVisible(..) runs the display loop
		if (fTakesFocusWhenVisible)
			fInformationControl.setFocus();
		if (fInformationControlCloser != null)
			fInformationControlCloser.start(subjectArea);
	}
//////////////////////////////	 

	We see that the Information control is set to visible. Maybe to create hovers,
	we need to work with this.
	-> 5 extensions of this interface exist
	 */
//}
