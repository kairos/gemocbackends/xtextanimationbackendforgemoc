package org.eclipse.gemoc.addon.xtextanimator.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.charts.ChartAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.comments.CommenterAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.comments.GrammarCommentStyle;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.highlights.HighlighterAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.hovers.HoverAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.hovers.HoverMouseLinker;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.markers.MarkerAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.properties.Properties;
import org.eclipse.gemoc.addon.xtextanimator.utils.AllClassRetriever;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.execution.concurrent.ccsljavaengine.concurrentmse.FeedbackMSE;
import org.eclipse.gemoc.execution.concurrent.ccsljavaengine.engine.AbstractSolverCodeExecutorConcurrentEngine;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.moc.ICCSLSolver;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.moc.ISolver;
import org.eclipse.gemoc.trace.commons.model.generictrace.GenericParallelStep;
import org.eclipse.gemoc.trace.commons.model.helper.StepHelper;
import org.eclipse.gemoc.trace.commons.model.trace.MSEOccurrence;
import org.eclipse.gemoc.trace.commons.model.trace.Step;
import org.eclipse.gemoc.xdsmlframework.api.core.EngineStatus.RunStatus;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.gemoc.xdsmlframework.api.engine_addon.IEngineAddon;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.trace.ModelElementReference;

/*
 * This class starts the animations of the plug-in
 */
public class XtextAnimationBehaviorManager implements IEngineAddon {
	
	private ISolver _solver;
	private HoverMouseLinker hoverMouseLinker;
	private List<Step<?>> allExecutedSteps = new ArrayList<>();
	private List<XtextAnimator> allAnimators = new ArrayList<>();
	
	/**
	 * TODO: solve this ! should be retrieved from the model
	 */
	public static final String language_ID = "org.lflang.LF"; //"org.eclipse.gemoc.example.moccmlsigpml.xtext.SigPML";
	//private String languageFullName; //languageFullName = engine.getExecutionContext().getLanguageDefinitionExtension().getName();	//not useful yet
	
	private HashMap<String, List<Properties>> configuration = new HashMap<>();
	
	@Override
	public void engineAboutToStart(IExecutionEngine<?> engine) {
		
		GrammarCommentStyle.languageId = language_ID;		
		RunStaRConfigurationRetriever configRetriever = new RunStaRConfigurationRetriever(engine);
		configuration = configRetriever.getConfiguration();
		this.hoverMouseLinker = new HoverMouseLinker();
		
		ArrayList<GemocClockEntity> clockList = new ArrayList<>();
		
		_solver = ((AbstractSolverCodeExecutorConcurrentEngine<?,?,?>)engine).getSolver();
		ArrayList<ModelElementReference> allClocks = ((ICCSLSolver)_solver).getAllDiscreteClocks();
		
		Resource res = null;
		loop1: for(ModelElementReference mer : allClocks){
			GemocClockEntity ced = new GemocClockEntity(mer);
			EObject associatedObject = ced.associatedObject; //Retrieve the associated object.
			if (mer.getElementRef().stream().filter(e -> (e instanceof Expression)).collect(Collectors.toList()).isEmpty()) {
				for(XtextAnimator xta : allAnimators) { //If we already have an animator with this clock : continue
//					if(xta.clockEntity.equals(ced)) {
//						continue loop1;
//					}
//					if(xta.associatedObject != null) {
//						if(xta.associatedObject.equals(associatedObject)) {
//							continue loop1; //to avoid duplicating animations
//						}
//					}
				}
				
				clockList.add(ced);
				if(res == null) {res = associatedObject.eResource();}
				// Gets the associated properties & creates the proper animators
				for(Class<?> clazz : AllClassRetriever.bfs(associatedObject.getClass())) {
					String className = clazz.getName();
					if(configuration.containsKey(className)) {
						List<Properties> associatedProperties = configuration.get(className);
						for(Properties prop: associatedProperties) {
							String value = prop.clockTrigger;
							if(ced.getName().contains(value) || value.equals(RunStaRConfigurationRetriever.ALL_CLOCKS)) {
								createAssociatedAnimators(prop, associatedObject, ced, engine);
							}
						}
						
							
						
					}
				}
				
			}
		}
		
		for(List<Properties> pList : configuration.values()) {
			for(Properties p : pList) {
				if (p.clockTrigger.equals(RunStaRConfigurationRetriever.ALL_CLOCKS)) {
					String linkedConceptName = p.className;
						if(res != null) {
							TreeIterator<EObject> it = res.getAllContents();
							while(it.hasNext()) {
								EObject eo = (EObject) it.next();
								if (linkedConceptName.compareTo(eo.getClass().getName())== 0) {
									createAssociatedAnimators(p, eo, null, engine);
								}
							}
						}	
								
					
				}
			}
		}
		
		
		//To provide secondary clocks (for the Chart for instance)
		for(GemocClockEntity clock : clockList) {
			for(XtextAnimator xta : allAnimators) {
				xta.checkAuxClocks(clock);
			}
		}
	}
	
	//Create the animators according to the defined properties
	private void createAssociatedAnimators(Properties property, EObject associatedObject, GemocClockEntity ced, IExecutionEngine<?> engine) {
		String imagePath = "";
		HoverAnimator manager = null;
		boolean needChartPath = false;
		XtextAnimator animator = property.createAnimator(associatedObject, ced, engine);
		allAnimators.add(animator);
		if(animator instanceof HoverAnimator) {
			manager = (HoverAnimator) animator;
			hoverMouseLinker.addManager(manager);
		}
		else if(animator instanceof ChartAnimator) {
			imagePath = ((ChartAnimator) animator).imagePath;
			needChartPath = true;
		}
		if(manager != null && needChartPath) {
			manager.imagePath = imagePath;
		}
	}
	
	@Override
	public void engineStarted(IExecutionEngine<?> engine) {
		hoverMouseLinker.setUpHovers();
	}
	
	/**
	 * For objects that are set to null after a step, and have their real value before
	 */
	//TODO : executer at all clocks + executer quand pas de clocks
	@Override
	public void aboutToExecuteStep(IExecutionEngine<?> engine, Step<?> stepToExecute) {
		if (stepToExecute instanceof GenericParallelStep) {
			MarkerAnimator.removeAllMarkers();
			commonEngineExecution(stepToExecute, false);
		}
	}
	
	@Override
	public void stepExecuted(IExecutionEngine<?> engine, Step<?> logicalStepExecuted) { 
		if (logicalStepExecuted instanceof GenericParallelStep) {
			System.out.println(logicalStepExecuted);
			allExecutedSteps.add(logicalStepExecuted);
			commonEngineExecution(logicalStepExecuted, true);
			
			HighlighterAnimator.showAllHighlights();
			for(XtextAnimator xta : allAnimators) {
				xta.previousStepClear();
			}
			CommenterAnimator.displayCodeMinings(); // Executing this AFTER highlight avoids their disappearance !!!
			allExecutedSteps.clear();
		}
	}
	
	@Override
	public void engineStatusChanged(IExecutionEngine<?> engine, RunStatus newStatus) {
//		if(newStatus == RunStatus.WaitingLogicalStepSelection) {
////			for(Step<?> s : allExecutedSteps) {
////				commonEngineExecution(s, true);
////			}
//			HighlighterAnimator.showAllHighlights();
//			for(XtextAnimator xta : allAnimators) {
//				xta.previousStepClear();
//			}
//			CommenterAnimator.displayCodeMinings(); // Executing this AFTER highlight avoids their disappearance !!!
//			allExecutedSteps.clear();
//		}
	}
	
	
	
	public void commonEngineExecution(Step<?> s, boolean stepExecution) {
		
		for(XtextAnimator xta : allAnimators) {
			if (xta.clockEntity == null) {
				if(xta.stepExecuted == stepExecution) {
					xta.updates();
				}
			}
		}
		
		for(XtextAnimator xta : allAnimators) {
			if (xta.clockEntity == null) { //do it after each step
				if(xta.stepExecuted == stepExecution) {
					xta.updates();
				}
			}
		}
		
		Stream<Object> t = allAnimators.stream().filter(an -> an instanceof HighlighterAnimator).map(ha -> ha.clockEntity);
		
		for(MSEOccurrence occ : StepHelper.collectAllMSEOccurrences(s)){
			if(occ.getMse() instanceof FeedbackMSE){
				Clock c = (Clock) ((FeedbackMSE)occ.getMse()).getFeedbackModelSpecificEvent().getSolverEvent();
				for(XtextAnimator xta : allAnimators) {
					if(xta.stepExecuted == stepExecution) {
						if(xta.clockEntity == null) {
							continue;
						}
						ConcreteEntity ce = ((GemocClockEntity)xta.clockEntity)._ce;
						if (ce.getName().compareTo(c.getName()) == 0){ 	
							xta.updates();
						}
					}
				}
			}
		}
		
		for(XtextAnimator xta : allAnimators) {
			if(xta.stepExecuted == stepExecution) {
				if(xta.clockEntity != null) {
					continue;
				}
				xta.updates();
			}
		}
	}
	
	@Override
	public void engineAboutToStop(IExecutionEngine<?> engine) {
		for(XtextAnimator xta : allAnimators) {
			xta.cleanUp();
		}
		MarkerAnimator.removeAllMarkers();
		hoverMouseLinker.clean();
	}
	
	@Override
	public void engineAboutToDispose(IExecutionEngine<?> engine) {

	}
	
}
