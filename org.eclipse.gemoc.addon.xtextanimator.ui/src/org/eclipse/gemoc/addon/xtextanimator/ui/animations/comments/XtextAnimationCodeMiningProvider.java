package org.eclipse.gemoc.addon.xtextanimator.ui.animations.comments;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.utils.DataRetriever;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.codemining.ICodeMining;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.service.CompoundModule;
import org.eclipse.xtext.ui.codemining.AbstractXtextCodeMiningProvider;
import org.eclipse.xtext.ui.editor.model.XtextDocumentUtil;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.util.IAcceptor;
import org.eclipse.xtext.util.concurrent.CancelableUnitOfWork;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

/*
 * Provides custom code mining
 */
@SuppressWarnings("restriction")
public class XtextAnimationCodeMiningProvider extends AbstractXtextCodeMiningProvider {

	private EObject associatedObject;
	private DataRetriever dataRetriever;
	private GrammarCommentStyle commentStyle;
	private List<String> attributesOrText;
	private String oldComment = "";
	private String newComment="";
	
	@Inject
	private XtextDocumentUtil xtextDocumentUtil;
	
	public XtextAnimationCodeMiningProvider(EObject associatedObject, List<String> attributesOrText, IExecutionEngine<?> e) {
		this.associatedObject = associatedObject;
		this.dataRetriever = new DataRetriever(e);
		this.attributesOrText = attributesOrText;
		this.commentStyle = new GrammarCommentStyle();
		Injector i = Guice.createInjector(new CompoundModule());
		i.injectMembers(this);
	}
	
	@Override
	public void createCodeMinings(IDocument document, XtextResource resource, CancelIndicator indicator, IAcceptor<? super ICodeMining> acceptor) throws BadLocationException {	
		EList<EObject> contents = resource.getContents();
		if (contents.isEmpty()) {
			return;
		}
		
		ICompositeNode node = NodeModelUtils.findActualNodeFor(associatedObject);
		int offset = node.getEndOffset();
		if(!oldComment.equals(newComment)) {
			oldComment = newComment;
			if(offset == document.getLength()) {
				int startLine = node.getStartLine();
				acceptor.accept(createNewLineHeaderCodeMining(startLine, document, newComment));
			}
			else {
				acceptor.accept(createNewLineContentCodeMining(offset, newComment));
			}
		}
	}
	
	/*
	 * The following method is called by the CodeMiningManager of the SourceViewer of the Editor
	 */
	@Override
	public CompletableFuture<List<? extends ICodeMining>> provideCodeMinings(ITextViewer viewer, IProgressMonitor monitor) {
		CompletableFuture<List<? extends ICodeMining>> future = CompletableFuture.supplyAsync(() -> {
			CancelableUnitOfWork<List<ICodeMining>, XtextResource> uow = new CancelableUnitOfWork<List<ICodeMining>, XtextResource>() {
				@Override
				public List<ICodeMining> exec(XtextResource resource, CancelIndicator uowCancelIndicator) throws Exception {
					return createCodeMinings(viewer.getDocument(), resource, uowCancelIndicator);
				}
			};
			return xtextDocumentUtil.getXtextDocument(viewer).tryReadOnly(uow, () -> Collections.emptyList());
		});
		return future;
	}
	
	public void updateComment() {
		newComment = newComment();
	}
	
	/*
	 * Writes as a language comment
	 */
	private String newComment() {
		return " " + commentStyle.correctMLComment(commentContent());
	}
	
	private String commentContent() {
		return dataRetriever.retrieveMultipleValues(associatedObject,attributesOrText);
	}

}
