package org.eclipse.gemoc.addon.xtextanimator.ui.animations.comments;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.jface.text.codemining.ICodeMiningProvider;
import org.eclipse.jface.text.source.AnnotationPainter;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.texteditor.DefaultMarkerAnnotationAccess;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.XtextSourceViewer;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;

/*
 * Exist only to be created as an XtextAnimator
 */
public class CommenterAnimator extends XtextAnimator {

	private XtextAnimationCodeMiningProvider provider;
	private static List<XtextAnimationCodeMiningProvider> providers = new ArrayList<>();
	private static AnnotationPainter painter;
	
	public CommenterAnimator(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e,	boolean stepExecuted, List<String> attributesTracked) {
		super(associatedObject, clockEntity, e, stepExecuted);
		provider = new XtextAnimationCodeMiningProvider(associatedObject, attributesTracked, e);
		providers.add(provider);
	}

	public XtextAnimationCodeMiningProvider getCodeMiningProvider() {
		return this.provider;
	}
	
	@Override
	public void updates() {
		provider.updateComment();
	}

	@Override
	public void cleanUp() {
		// TODO Auto-generated method stub
		
	}

	public static void displayCodeMinings() {		
		Display.getDefault().syncExec(new Runnable() {
			@Override
		    public void run() {
				XtextEditor editor = EditorUtils.getActiveXtextEditor();
				XtextSourceViewer viewer = (XtextSourceViewer) editor.getInternalSourceViewer();
				if(painter == null) {
					DefaultMarkerAnnotationAccess annotationAccess = new DefaultMarkerAnnotationAccess();
					painter = new AnnotationPainter(viewer, annotationAccess);			
					viewer.setCodeMiningAnnotationPainter(painter);
				}
				int size = providers.size();
				ICodeMiningProvider[] codeMiningProviders = new ICodeMiningProvider[size];
				for(int i = 0; i < size; i++) {
					codeMiningProviders[i] = providers.get(i);
				}
				viewer.setCodeMiningProviders(codeMiningProviders);
			}
		});
	}

	@Override
	public void previousStepClear() {
		// TODO Auto-generated method stub
		
	}


	
}
