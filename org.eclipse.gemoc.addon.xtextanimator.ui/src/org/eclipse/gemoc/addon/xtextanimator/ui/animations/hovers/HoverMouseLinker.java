package org.eclipse.gemoc.addon.xtextanimator.ui.animations.hovers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.utils.XtextAnimationMouseListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.XtextSourceViewer;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;

/**
 * The desired behavior is to display a hover on double click on a variable
 * This class retrieves the different associated objects and provides them to
 * the Mouse Listener, in order to create the appropriate hover
 */
public class HoverMouseLinker {
	
	private List<EObject> associatedObjects;
	private List<HoverAnimator> hoverManagers;
	private XtextAnimationMouseListener listener;
	
	public HoverMouseLinker() {
		this.hoverManagers = new ArrayList<>();
		this.associatedObjects = new ArrayList<>();
	}
	
	public void setUpHovers() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
		    public void run() {
				setUpManagers();
				listener = new XtextAnimationMouseListener(associatedObjects,hoverManagers);
				XtextEditor editor = EditorUtils.getActiveXtextEditor();
				XtextSourceViewer viewer = (XtextSourceViewer) editor.getInternalSourceViewer();	
				viewer.getTextWidget().addMouseListener(listener);
			}
		});
	}
	
	public void addManager(HoverAnimator manager) {
		associatedObjects.add(manager.associatedObject);
		hoverManagers.add(manager);
	}
	
	private void setUpManagers() {
		for(HoverAnimator manager : hoverManagers) {
			manager.setUpHover();
		}
	}
	
	public void clean() {
		removeListener();
		removeManagers();
	}
	
	/*
	 * The listener needs to be disposed of at the end of each session
	 */
	private void removeListener() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
		    public void run() {
				XtextEditor editor = EditorUtils.getActiveXtextEditor();
				XtextSourceViewer viewer = (XtextSourceViewer) editor.getInternalSourceViewer();	
				viewer.getTextWidget().removeMouseListener(listener);
			}
		});
	}
	
	private void removeManagers() {
		this.hoverManagers.clear();
	}

}
