package org.eclipse.gemoc.addon.xtextanimator.ui.animations.charts;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle;

public class ChartObject {

	private String folderPath;
	private String graphTitle;
	private String xLabel;
	private String yLabel;
	private String xAttribute;
	private String yAttribute;
	private int nbValues;
	private LineStyle style;
	
	public ChartObject(ChartVar chart) {
		setFolderPath(chart.getPath());
		setGraphTitle(chart.getTitle());
		setXLabel(chart.getXlabel());
		setYLabel(chart.getYlabel());
		setxAttribute(chart.getXrtd().getAttributes().get(0));
		setyAttribute(chart.getYrtds().get(0).getAttributes().get(0));
		setNbValues(chart.getNbValuesDisplayed());
		setStyle(chart.getLineStyle());
	}

	public String getFolderPath() {
		return folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	public String getGraphTitle() {
		return graphTitle;
	}

	public void setGraphTitle(String graphTitle) {
		this.graphTitle = graphTitle;
	}

	public String getXLabel() {
		return xLabel;
	}

	public void setXLabel(String xLabel) {
		this.xLabel = xLabel;
	}

	public String getYLabel() {
		return yLabel;
	}

	public void setYLabel(String yLabel) {
		this.yLabel = yLabel;
	}

	public LineStyle getStyle() {
		return style;
	}

	public void setStyle(LineStyle style) {
		this.style = style;
	}

	public int getNbValues() {
		return nbValues;
	}

	public void setNbValues(int nbValues) {
		this.nbValues = nbValues;
	}
	
	public String getxAttribute() {
		return xAttribute;
	}
	
	public void setxAttribute(String xAttribute) {
		this.xAttribute = xAttribute;
	}
	
	public String getyAttribute() {
		return yAttribute;
	}

	public void setyAttribute(String yAttribute) {
		this.yAttribute = yAttribute;
	}

}