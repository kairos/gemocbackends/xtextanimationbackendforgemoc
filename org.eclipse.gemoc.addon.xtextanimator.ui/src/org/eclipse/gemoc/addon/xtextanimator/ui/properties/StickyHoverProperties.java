package org.eclipse.gemoc.addon.xtextanimator.ui.properties;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.hovers.HTMLBuilder;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.hovers.HoverAnimator;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;

/*
 * Properties concerning permanent hovers.
 * Sticky hovers appear by double clicking on an object of interest.
 * Different kinds of information can be displayed :
 * * the current value of the tracked attribute
 * * the evolution of the tracked attribute over time (graph)
 * * text can be displayed in a specific color
 */
public class StickyHoverProperties extends Properties {

	private List<String> attributesTracked;
	private String imagePath;
	private HTMLBuilder htmlBuilder;
	private boolean fromStart;
	private boolean displayText;
	
	public StickyHoverProperties(String className, String clockTrigger, boolean stepExecuted,
								 List<String> attributesTracked, String imagePath, HTMLBuilder htmlBuilder, boolean fromStart, boolean displayText) {
		super(className,clockTrigger,stepExecuted);
		this.attributesTracked = attributesTracked;
		this.imagePath = imagePath;
		this.htmlBuilder = htmlBuilder;
		this.fromStart = fromStart;
		this.displayText = displayText;
	}

	@Override
	public XtextAnimator createAnimator(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e) {
		return createStickyHoverManager(associatedObject, clockEntity, e);
	}
	
	private HoverAnimator createStickyHoverManager(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e) {		
		return new HoverAnimator(associatedObject, clockEntity, e, stepExecuted, attributesTracked, htmlBuilder, imagePath, fromStart, displayText);
	}
}
