package org.eclipse.gemoc.addon.xtextanimator.ui.properties;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.charts.ChartAnimator;
import org.eclipse.gemoc.addon.xtextanimator.ui.animations.charts.ChartObject;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;

/*
 * Properties concerning graphs.
 * Evolution of tracked objects can be visualized over time.
 * A graph is created in the defined folder, displaying
 * the evolution of the tracked attribute 
 */
public class ChartProperties extends Properties {

	private ChartObject chart;
	private String xClassName;
	
	public ChartProperties(String yClassName, String yClockTrigger, boolean stepExecuted,
						   ChartObject chart, String xClassName) {
		super(yClassName, yClockTrigger, stepExecuted);
		this.xClassName = xClassName;
		this.chart = chart;
	}

	@Override
	public XtextAnimator createAnimator(EObject yObject, GemocClockEntity clockEntity, IExecutionEngine<?> e) {
		ChartAnimator graph = new ChartAnimator(yObject, clockEntity, e, stepExecuted, xClassName, chart);
		return graph;
	}

}
