package org.eclipse.gemoc.addon.xtextanimator.ui.animations.runtoline;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;

/**
 * Brings the cursor to the beginning of the associatedObject's line
 */
public class ToLineRunner extends XtextAnimator {

	public ToLineRunner(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e, boolean stepExecuted) {
		super(associatedObject, clockEntity,e, stepExecuted);
	}

	@Override
	public void updates() {
		goToElement();
	}
	
	@Override
	public void cleanUp() {
		//ignored, nothing to be cleaned
	}
	
	private void goToElement() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
		    public void run() {
				XtextEditor editor = EditorUtils.getActiveXtextEditor();
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				INode node = NodeModelUtils.getNode(associatedObject);
				int offset = node.getOffset();
				int length = node.getLength();
				int line = node.getStartLine()>0? (node.getStartLine()-1) : 0;
				IXtextDocument document = editor.getDocument();
				   try {
					   editor.setHighlightRange(offset, length, true);
					   int lineStart = document.getLineOffset(line);
					   editor.selectAndReveal(lineStart, 0);
					   page.activate(editor);
				   } catch (BadLocationException x) {
					        // ignore
				   }
			}
		});
	}

	@Override
	public void previousStepClear() {
		// TODO Auto-generated method stub
		
	}


}
