package org.eclipse.gemoc.addon.xtextanimator.ui;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.google.inject.Injector;

/**
 * The activator class controls the plug-in life cycle
 */
public class ActivatorXtA extends AbstractUIPlugin{
	
	// The plug-in ID
	public static final String PLUGIN_ID = "org.eclipse.gemoc.addon.xtextanimator";

	// The shared instance
	private static ActivatorXtA plugin;
	
	private Injector injector;
	
	public ActivatorXtA() {}
	
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}
	
	public Injector getInjector() {
		return injector;
	}

	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static ActivatorXtA getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	 public Image createImage(String path) {
		   Image image = getImageRegistry().get(path);
		   if (image == null) {
		     getImageRegistry().put(path, AbstractUIPlugin.
		       imageDescriptorFromPlugin("id", path));
		     image = getImageRegistry().get(path);
		   }
		   return image;
		 }
}
