package org.eclipse.gemoc.addon.xtextanimator.ui.animations.highlights;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.jface.text.TextPresentation;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.XtextPresentationReconciler;
import org.eclipse.xtext.ui.editor.XtextSourceViewer;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;

/*
 * Highlights element inside the editor.
 */
public class HighlighterAnimator extends XtextAnimator {
	
	public Color highlightColor = null;
	public boolean background = true;
	public boolean isPersistent;
	private int startOffset;
	private int length;
	
	//Dans runstar on peut create: Paint(clear) for lf.Output after nia
	//on peut rajouter un removeRangeWhen : string trigger
	//faire FOREGROUND / BACKRGOU
	
	/*
	 * Each of our associated objects, if they need highlighting,
	 * will have its own style range created by this animator.
	 * All style ranges are put in a shared list in order to highlight
	 * everyone at the same time, when all other actions are resolved.
	 * This is done to avoid disappearing highlight 
	 */
	public class ExtendedStyleRange extends StyleRange{public HighlighterAnimator associatedAnimator;};
	public static List<ExtendedStyleRange> ranges = new ArrayList<>();
	
	public HighlighterAnimator(EObject associatedObject, GemocClockEntity ce, IExecutionEngine<?> e, boolean stepExecuted, Color hc, boolean bg, boolean isP) {
		super(associatedObject, ce, e, stepExecuted);
		this.highlightColor = hc;
		this.background = bg;
		this.isPersistent = isP;
		INode node = NodeModelUtils.getNode(associatedObject);
		startOffset = node.getOffset();
		length = node.getLength();
	}
	
	@Override
	public void updates() {
		highlightElements();
	}

	@Override
	public void cleanUp() {	
		ranges.clear();
	}
	
	public void highlightElements() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
		    public void run() {
				if (highlightColor == null) {
					ranges.removeIf(style -> style.associatedAnimator.associatedObject == associatedObject);
					return;
				}
				
				XtextEditor editor = EditorUtils.getActiveXtextEditor();
				IXtextDocument document = editor.getDocument();
				XtextSourceViewer viewer = (XtextSourceViewer) editor.getInternalSourceViewer();
				
				XtextPresentationReconciler pr = getPresentationReconciler(viewer);
				
				
				StyledText widget = viewer.getTextWidget();
				Shell shell = widget.getShell();
				ranges.add(createStyleRange(shell, document));
			}
		});
	}
	
	/*
	 * As syntax coloring is also a Style Range, we need to add our own style ranges
	 * carefully in order to avoid collision.
	 * The TextPresentation object allows us to do so : it computes and solves collisions
	 * on its own
	 */
	public static void showAllHighlights() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
		    public void run() {
				XtextEditor editor = EditorUtils.getActiveXtextEditor();
				TextPresentation textPres = new TextPresentation();
				editor.getInternalSourceViewer().invalidateTextPresentation();
				if(!ranges.isEmpty()) {
					for(StyleRange s : ranges) {
						textPres.mergeStyleRange(new StyleRange(s.start, s.length, s.foreground, s.background, SWT.BOLD));
					}
					editor.getInternalSourceViewer().changeTextPresentation(textPres, false);
				}
			}
		});
	}
	
	private ExtendedStyleRange createStyleRange(Shell shell, IXtextDocument document) {
		
		if(background) {
			return backgroundStyleRange(shell);
		}
		else {
			return foregroundStyleRange(shell);
		}
	}
	
	/**
	 * Highlighting elements with style ranges
	 * @param shell is the parent shell
	 * @param offset is the offset of the considered node
	 * @param length is the length of the considered node
	 * @return a style range with highlighting
	 */
	private ExtendedStyleRange backgroundStyleRange(Shell shell) {
		ExtendedStyleRange sr = new ExtendedStyleRange();
		sr.start = startOffset;
		sr.length = length;
		if(highlightColor != null) sr.background = highlightColor;
		sr.associatedAnimator = this;
		return sr;
	}
	
	private ExtendedStyleRange foregroundStyleRange(Shell shell) {
		ExtendedStyleRange sr = new ExtendedStyleRange();
		sr.start = startOffset;
		sr.length = length;
		if(highlightColor != null) sr.foreground = highlightColor;
		sr.associatedAnimator = this;
		return sr;
	}

	@Override
	public void previousStepClear() {
		if (ranges.isEmpty()) {
			return;
		}
		if (!isPersistent) {
			ranges.removeIf(style -> style.associatedAnimator == this);
		}
	}
	
	
	/**
	 * Returns the {@link TMPresentationReconciler} of the given text viewer and
	 * null otherwise.
	 *
	 * @param textViewer
	 * @return the {@link TMPresentationReconciler} of the given text viewer and
	 *         null otherwise.
	 */
	public static XtextPresentationReconciler getPresentationReconciler(XtextSourceViewer textViewer) {
		try {
			Field field = SourceViewer.class.getDeclaredField("fPresentationReconciler");
			if (field != null) {
				field.setAccessible(true);
				IPresentationReconciler presentationReconciler = (IPresentationReconciler) field.get(textViewer);
				return presentationReconciler instanceof XtextPresentationReconciler
						? (XtextPresentationReconciler) presentationReconciler
						: null;
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return null;
	}

}
