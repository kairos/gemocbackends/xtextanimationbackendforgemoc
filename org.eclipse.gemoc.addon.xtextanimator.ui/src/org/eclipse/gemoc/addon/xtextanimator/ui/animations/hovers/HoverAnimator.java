package org.eclipse.gemoc.addon.xtextanimator.ui.animations.hovers;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.addon.xtextanimator.ui.XtextAnimator;
import org.eclipse.gemoc.addon.xtextanimator.utils.GemocClockEntity;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

/*
 * The following manager is responsible for a SWT window 
 * popping on double click.
 */
public class HoverAnimator extends XtextAnimator {

	private List<String> attributesTracked;
	private boolean hasImage;
	private boolean fromStart;
	private boolean createBrowser;
	
	private HTMLBuilder htmlBuilder;
	public String imagePath;
	private Shell shell;
	private Browser browser;
	private Label label;
	
	private boolean setVisible = false;
	
//	private String javascriptExample = "<!DOCTYPE html>\n"
//			+ "<html>\n"
//			+ "<body>\n"
//			+ "<p id=\"demo\">A Paragraph.</p>\n"
//			+ "\n"
//			+ "<button type=\"button\" onclick=\"myFunction()\">Try it</button>\n"
//			+ "\n"
//			+ "<script>\n"
//			+ "function myFunction() {\n"
//			+ "  document.getElementById(\"demo\").innerHTML = \"Paragraph changed.\";\n"
//			+ "}\n"
//			+ "</script>\n"
//			+ "\n"
//			+ "</body>\n"
//			+ "</html> ";
	
	public HoverAnimator(EObject associatedObject, GemocClockEntity clockEntity, IExecutionEngine<?> e, boolean stepExecuted, 
			  List<String> attributesTracked,  HTMLBuilder htmlBuilder, String imagePath, boolean fromStart, boolean displayText) {
		super(associatedObject, clockEntity, e, stepExecuted);
		this.attributesTracked = attributesTracked;
		if (imagePath.compareTo("./") == 0) {
			this.imagePath = imagePath+associatedObject.hashCode()+ ".png";;
		}else {
			this.imagePath = imagePath;
		}
		
		this.hasImage = !(imagePath.length() == 0);
		this.htmlBuilder = htmlBuilder;
		this.fromStart = fromStart;
		this.createBrowser = displayText;
	}
	
	@Override
	public void updates() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
		    public void run() {
				updateInformation();
			}
		});
	}

	@Override
	public void cleanUp() {
		if(!shell.isDisposed()) getShell().dispose();
	}

	public void setUpHover() {	
		Display.getDefault().syncExec(new Runnable() {
			@Override
		    public void run() {
				initShell();
				if(createBrowser) { //Browsers are suited for text / javascript / html
					initBrowser();
				}
				if(hasImage) { //Create a Label : they are the most suited to handle images
					initLabel();	
				}
				if(fromStart) {
					Point location = new Point(100,100);
					displayHover(location);
					fromStart = false;
				} else {
					updateInformation();
				}
			}
		});
	}
	
	public void displayHover(Point location) {
		if(shell.isDisposed()) {
			initShell();
		}
		shell.setLocation(location.x,location.y);
		setVisible = true;
		updateInformation();
		shell.setVisible(true);
		shell.pack();
		shell.open();
	}
	
	public Shell getShell() {
		return shell;
	}

	public void setShell(Shell shell) {
		this.shell = shell;
	}
	
	private void updateInformation() {
		String information = getHoverInfoAsHtml();
		if(shell == null) {
			initShell();
		}
		if(setVisible) {
			if(browser != null) {
				if(browser.isDisposed()) initBrowser();
				browser.setText(information);
				if(!browser.isVisible()) browser.setVisible(true);
				browser.redraw();
			}
			if(hasImage) {
				try {
					if(label.isDisposed()) initLabel();
					Image image = new Image(getShell().getDisplay(),imagePath);
					label.setImage(image);
					Point s = label.getSize();
					s.x/=2;
					s.y/=2;
					label.setSize(s);
					label.setAlignment(SWT.CENTER);
					if(!label.isVisible()) label.setVisible(true);
					label.redraw();
				} catch(Exception e) {
					System.err.println("problem in hover update information: "+e);
				}		
			}
			shell.update();
		}
	}
	
	private void initShell() {
		setShell(new Shell(Display.getCurrent(), SWT.ON_TOP | SWT.SHELL_TRIM));
		String title = associatedObject.eClass().getName() + " " + dataRetriever.getName(associatedObject);
		shell.setText(title);
		shell.addListener(SWT.CLOSE, new Listener() {
			@Override
			public void handleEvent(Event event) {
				shell.setVisible(false);	
			}
		});
		//The contents fill the window, event when resized
		FillLayout layout = new FillLayout();
	       // Optionally set layout fields.
	       layout.type = SWT.VERTICAL;
	       layout.spacing = 2;
	       layout.marginWidth = 2;
	       layout.marginHeight = 2;
		   // Set the layout into the composite.
	       shell.setLayout(layout);
		setShellSize();
		shell.pack();
	}
	
	private void initLabel() {
		label = new Label(shell, SWT.NONE);
		label.setVisible(false);
	}
	
	private void initBrowser() {
		browser = new Browser(shell,SWT.NONE);
//		//The contents fill the window, event when resized
		RowLayout layout = new RowLayout();
		layout.pack = true;	
		browser.setLayout(layout);	//Set size works only if we're not using layout
		browser.setJavascriptEnabled(true);
		browser.setVisible(false);
		browser.pack();
	}
	
	private void setShellSize() {
		//To size according to its contents
		shell.layout();
		Point size = shell.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		shell.setSize(Math.max(size.x, 100), Math.max(size.y, 100));
	}
	
	private String getHoverInfoAsHtml(){
		String text = getHoverInfo();
		String result = toHTML(text);
		return result;
	}
	
	private String toHTML(String text) {
		return this.htmlBuilder.inHTML(text);
	}
	
	private String getHoverInfo() {
		return dataRetriever.retrieveMultipleValues(associatedObject,attributesTracked);
	}

	@Override
	public void previousStepClear() {
		// TODO Auto-generated method stub
		
	}

}